/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Universidad	de	los	Andes	(Bogotá	- Colombia)
 * Departamento	de	Ingeniería	de	Sistemas	y	Computación
 * Licenciado	bajo	el	esquema	Academic Free License versión 2.1
 * 		
 * Curso: isis2304 - Sistemas Transaccionales
 * Proyecto: Parranderos Uniandes
 * @version 1.0
 * @author Germán Bravo
 * Julio de 2018
 * 
 * Revisado por: Claudia Jiménez, Christian Ariza
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto BAR de Parranderos
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author Germán Bravo
 */
class SQLUtil
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes pp;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLUtil (PersistenciaSuperAndes pp)
	{
		this.pp = pp;
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para obtener un nuevo número de secuencia
	 * @param pm - El manejador de persistencia
	 * @return El número de secuencia generado
	 */
	public long nextval (PersistenceManager pm)
	{
        Query q = pm.newQuery(SQL, "SELECT "+ pp.darSeqSuperAndes () + ".nextval FROM DUAL");
        q.setResultClass(Long.class);
        long resp = (long) q.executeUnique();
        return resp;
	}

	/**
	 * Crea y ejecuta las sentencias SQL para cada tabla de la base de datos - EL ORDEN ES IMPORTANTE 
	 * @param pm - El manejador de persistencia
	 * @return Un arreglo con 7 números que indican el número de tuplas borradas en las tablas GUSTAN, SIRVEN, VISITAN, BEBIDA,
	 * TIPOBEBIDA, BEBEDOR y BAR, respectivamente
	 */
	public long [] limpiarSuperAndes (PersistenceManager pm)
	{
        Query qVentaEmpresa = pm.newQuery(SQL, "DELETE FROM " + pp.darTablaVenta());
        Query qPedido = pm.newQuery(SQL, "DELETE FROM " + pp.darTablaPedido());
        Query qPromocion = pm.newQuery(SQL, "DELETE FROM " + pp.darTablaPromocion());
        Query qInsumo = pm.newQuery(SQL, "DELETE FROM " + pp.darTablaInsumo());
        Query qProducto = pm.newQuery(SQL, "DELETE FROM " + pp.darTablaProducto());
        Query qProveedor = pm.newQuery(SQL, "DELETE FROM " + pp.darTablaProveedor());
        Query qEstante = pm.newQuery(SQL, "DELETE FROM " + pp.darTablaEstante());
        Query qBodega = pm.newQuery(SQL, "DELETE FROM " + pp.darTablaBodega());
        Query qSucursal = pm.newQuery(SQL, "DELETE FROM " + pp.darTablaSucursal());

        long ventasEmpresaEliminadas = (long) qVentaEmpresa.executeUnique ();
        long pedidosEliminados = (long) qPedido.executeUnique ();
        long promocionesEliminadas = (long) qPromocion.executeUnique ();
        long insumosEliminados = (long) qInsumo.executeUnique ();
        long productosEliminados = (long) qProducto.executeUnique ();
        long proveedoresEliminados = (long) qProveedor.executeUnique ();
        long estantesEliminadas = (long) qEstante.executeUnique ();
        long bodegasEliminadas = (long) qBodega.executeUnique ();
        long sucursalesEliminadas = (long) qSucursal.executeUnique ();
        return new long[] {ventasEmpresaEliminadas, pedidosEliminados, promocionesEliminadas,
							insumosEliminados, productosEliminados, proveedoresEliminados,
							estantesEliminadas, bodegasEliminadas, sucursalesEliminadas};
	}

}
