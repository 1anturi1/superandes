package persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import negocio.Estante;

public class SQLEstante {
	
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes pp;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLEstante (PersistenciaSuperAndes pp)
	{
		this.pp = pp;
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de BODEGAS de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos BODEGA
	 */
	public List<Estante> darEstantes(PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + pp.darTablaEstante());
		q.setResultClass(Estante.class);
		return (List<Estante>) q.executeList();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un ESTANTE a la base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @param idSucursal - El identificador de la sucursal a la cual pertenece el estante
	 * @param id - El identificador del estante
	 * @param cantidadActual - La cantidad de productos que hay en la blodega
	 * @param capacidadPeso - La capasidad en peso de la bodega
	 * @param capacidadVolumen - La capasidad en volumen de la bodega
	 * @param nivelAbastecimiento -  El nivel de abastecimiento del estante
	 * @param tipoProducto - El tipo de producto de la bodega
	 * @return EL número de tuplas insertadas
	 */
	public long registrarEstante(PersistenceManager pm, int idSucursal, int id, int cantidadActual, int capacidadPeso, int capacidadVolumen, int nivelAbastecimiento, String tipoProducto)
	{
		String pQuery = "(id, capacidad_peso, capacidad_vol, categoria_producto, cantidad_productos, cant_actual, nivel_abastecimiento, id_sucursal) values (?, ?, ?, ?, ?, ?, ?, ?)";
		Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaProducto () + pQuery);
		q.setParameters(id, capacidadPeso, capacidadVolumen, tipoProducto, cantidadActual, cantidadActual, nivelAbastecimiento, idSucursal);
		return (long) q.executeUnique();
	}

}
