package persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLCarrito_Insum 
{
	  /* ****************************************************************
     * 			Constantes
     *****************************************************************/
    /**
     * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
     * Se renombra acá para facilitar la escritura de las sentencias
     */
    private final static String SQL = PersistenciaSuperAndes.SQL;

    /* ****************************************************************
     * 			Atributos
     *****************************************************************/
    /**
     * El manejador de persistencia general de la aplicación
     */
    private PersistenciaSuperAndes pp;

    /* ****************************************************************
     * 			Métodos
     *****************************************************************/

    /**
     * Constructor
     * @param pp - El Manejador de persistencia de la aplicación
     */
    public SQLCarrito_Insum (PersistenciaSuperAndes pp)
    {
        this.pp = pp;
    }
	
    
	public long adicionarProductoACarrito(PersistenceManager pm, int idCarrito, int codigoBarrasInsumo)
	{
		String pQuery = "(ID_CARRITO, ID_INSUM) values (?, ?)";
		Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaCarritoInsum() + pQuery);
		q.setParameters(idCarrito, codigoBarrasInsumo);
		return (long) q.executeUnique();
	}
	
	
	public int eliminarProductoACarrito(PersistenceManager pm,  int idCarrito, int codigoBarrasInsumo)
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + pp.darTablaCarritoInsum() + " WHERE ID_CARRITO = ? AND  ID_INSUM = ?");
		q.setParameters(idCarrito, codigoBarrasInsumo);
		return (int) q.executeUnique();
	}
	
	
	
	
	
}
