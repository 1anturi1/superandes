package persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import negocio.Bodega;
import negocio.Consumidor;

public class SQLBodega 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes pp;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLBodega (PersistenciaSuperAndes pp)
	{
		this.pp = pp;
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de BODEGAS de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos BODEGA
	 */
	public List<Bodega> darBodegas(PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + pp.darTablaBodega ());
		q.setResultClass(Bodega.class);
		return (List<Bodega>) q.executeList();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para adicionar una BODEGA a la base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @param idSucursal - El identificador de la sucursal a la cual pertenece la bodega
	 * @param id - El identificador de la bodega
	 * @param cantidadActual - La cantidad de productos que hay en la blodega
	 * @param capacidadPeso - La capasidad en peso de la bodega
	 * @param capacidadVolumen - La capasidad en volumen de la bodega
	 * @param tipoProducto - El tipo de producto de la bodega
	 * @return EL número de tuplas insertadas
	 */
	public long registrarBodega(PersistenceManager pm, int idSucursal, int id, int cantidadActual, int capacidadPeso, int capacidadVolumen, String tipoProducto)
	{
		String pQuery = "(id, capacidad_peso, capacidad_vol, categoria_producto, cant_actual, id_sucursal) values (?, ?, ?, ?, ?, ?)";
		Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaProducto () + pQuery);
		q.setParameters(id, capacidadPeso, capacidadVolumen, tipoProducto, cantidadActual, idSucursal);
		return (long) q.executeUnique();
	}

		/**
		 * Crea y ejecuta la sentencia SQL para encontrar la información de una BODEGA de la
		 * base de datos de SuperAndes, por su tipo
		 * @param pm - El manejador de persistencia
		 * @param tipo - El tipo de la bodega
		 * @return El id de la BODEGA que tiene el tipo dado
		 */
		public int darBodegaPorTipo (PersistenceManager pm, String tipo)
		{
			Query q = pm.newQuery(SQL, "SELECT id FROM " + pp.darTablaBodega() + " WHERE categoria_producto = ?");
			//q.setResultClass(Bodega.class);
			q.setParameters(tipo);
			return (int) q.executeUnique();
		}
}
