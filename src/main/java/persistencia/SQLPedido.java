package persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import negocio.Pedido;

public class SQLPedido {

	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes pp;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLPedido (PersistenciaSuperAndes pp)
	{
		this.pp = pp;
	}
	
	/**

	 * Crea y ejecuta la sentencia SQL para encontrar la información de Pedido de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos PEDIDO

	*/
	public List<Pedido> darPedidos(PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + pp.darTablaPedido ());
		q.setResultClass(Pedido.class);
		return (List<Pedido>) q.executeList();
	}

	/**
	 * RF9
	 * @param pm
	 * @param nitProveedor
	 * @param codProducto
	 */
	public void registrarPedidoEnSucursal(PersistenceManager pm,int nitProveedor, int codProducto)
	{
		
		Query q = pm.newQuery(SQL," SELECT Precio from Producto	where Codigo_barras =" + codProducto );
		int precio = (int)q.executeUnique() ;
		
		Query f = pm.newQuery(SQL, "insert into pedido (id, costo, id_prov, cod_producto, estado) values (" + 65 + "," + precio + "," + nitProveedor + "," + codProducto+ "," + " 'proceso' " +")");
	
	}
	/**
	 * RF10
	 * @param pm
	 * @param idPedido
	 */
	
	public void registrarLlegadaDePedido(PersistenceManager pm,int idPedido)
	{
		Query q = pm.newQuery(SQL," UPDATE pedido SET estado = 'recibido' WHERE id = " +idPedido  );
		
		Query f = pm.newQuery(SQL, "Select cod_producto from pedido where id =" + idPedido) ;
		
		int codi = (int)f.executeUnique() ;
		
		Query b = pm.newQuery(SQL, " UPDATE inventario SET cant_total = cant_total + 1 WHERE cod_producto = " +codi ) ;
		
	}
	
	
	
}
