package persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import negocio.Producto;

public class SQLProducto {

	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes pp;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLProducto (PersistenciaSuperAndes pp)
	{
		this.pp = pp;
	}

	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un PRODUCTO a la base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @param codigoDeBarras - El identificador del producto
	 * @param nombre - El nombre del producto
	 * @param precio - El precio del producto
	 * @param marca - La marca del producto
	 * @param calificacionCalidad - La clasificación de calodad del producto
	 * @param presentacion - La presentación del producto
	 * @param cantidadPresentacion - La cantidad por presentación del producto
	 * @param clasificacion - La clasificación del producto
	 * @param precioPorUnidadDeMedida - El precio por unidad de medida del producto
	 * @param tipoProducto - El tipo del producto
	 * @param unidadDeMedida - La unidad de medida del producto
	 * @return EL número de tuplas insertadas
	 */
	public long adicionarProducto (PersistenceManager pm, int codigoDeBarras,
								   String nombre, int precio, String marca,
								   double calificacionCalidad, String presentacion,
								   int cantidadPresentacion, String clasificacion,
								   int precioPorUnidadDeMedida, String tipoProducto,
								   String unidadDeMedida)
	{
		String pQuery = "(codigo_barras, calificacion_calidad, cantidad_present, clasificacion, marca, nombre, precio, precio_unit, presentacion, tipo, unit_medida) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaProducto () + pQuery);
		q.setParameters(codigoDeBarras, calificacionCalidad, cantidadPresentacion, clasificacion, marca, nombre, precio, precioPorUnidadDeMedida, presentacion, tipoProducto, unidadDeMedida);
		return (long) q.executeUnique();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de BODEGAS de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos BODEGA
	 */
	public List<Producto> darProductos(PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + pp.darTablaProducto ());
		q.setResultClass(Producto.class);
		return (List<Producto>) q.executeList();
	}
}
