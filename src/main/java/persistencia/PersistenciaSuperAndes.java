package persistencia;

import java.sql.Date;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.jdo.JDODataStoreException;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Transaction;

import org.apache.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import negocio.Bodega;
import negocio.Estante;
import negocio.Insumo;
import negocio.Pedido;
import negocio.Consumidor;
import negocio.Producto;
import negocio.Promocion;
import negocio.Proveedor;
import negocio.Sucursal;
import negocio.Venta;


public class PersistenciaSuperAndes {
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Logger para escribir la traza de la ejecución
	 */
	private static Logger log = Logger.getLogger(PersistenciaSuperAndes.class.getName());

	/**
	 * Cadena para indicar el tipo de sentencias que se va a utilizar en una consulta
	 */
	public final static String SQL = "javax.jdo.query.SQL";

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * Atributo privado que es el único objeto de la clase - Patrón SINGLETON
	 */
	private static PersistenciaSuperAndes instance;

	/**
	 * Fábrica de Manejadores de persistencia, para el manejo correcto de las transacciones
	 */
	private PersistenceManagerFactory pmf;

	/**
	 * Arreglo de cadenas con los nombres de las tablas de la base de datos, en su orden:
	 * Secuenciador, tipoBebida, bebida, bar, bebedor, gustan, sirven y visitan
	 */
	private List<String> tablas;

	/**
	 * Atributo para el acceso a las sentencias SQL propias a PersistenciaSuperandes
	 */
	private SQLUtil sqlUtil;

	/**
	 * Atributo para el acceso a la tabla BODEGA_INSUMO de la base de datos
	 */
	private SQLBodega_Insumo sqlBodega_Insumo;

	/**
	 * Atributo para el acceso a la tabla BODEGA de la base de datos
	 */
	private SQLBodega sqlBodega;
	/**
	 * Atributo para el acceso a la tabla ESTANTE de la base de datos
	 */
	private SQLEstante sqlEstante;
	
	/**
	 * Atributo para el acceso a la tabla INSUMO de la base de datos
	 */
	private SQLInsumo sqlInsumo;
	
	/**
	 * Atributo para el acceso a la tabla PEDIDO de la base de datos
	 */
	private SQLPedido sqlPedido;
	
	/**
	 * Atributo para el acceso a la tabla PRODUCTO de la base de datos
	 */
	private SQLProducto sqlProducto;
	
	/**
	 * Atributo para el acceso a la tabla PROMOCION de la base de datos
	 */
	private SQLPromocion sqlPromocion;
	
	/**
	 * Atributo para el acceso a la tabla PROVEEDOR de la base de datos
	 */
	private SQLProveedor sqlProveedor;

	/**
	 * Atributo para el acceso a la tabla CONSUMIDOR de la base de datos
	 */
	private SQLConsumidor sqlConsumidor;

	/**
	 * Atributo para el acceso a la tabla SUCURSAL de la base de datos
	 */
	private SQLSucursal sqlSucursal;
	

	
	private SQLCarrito_Insum sqlCarritoInsumo ;
	

	/**
	 * Atributo para el acceso a la tabla VENTA_CONSUMIDOR_EMP de la base de datos
	 */
	private SQLVenta sqlVenta;

	/**
	 * Atributo para el acceso a la tabla Carrito de la base de datos
	 */
	private SQLCarrito sqlCarrito;

	
	
	/* ****************************************************************
	 * 			Métodos del MANEJADOR DE PERSISTENCIA
	 *****************************************************************/

	/**
	 * Constructor privado con valores por defecto - Patrón SINGLETON
	 */
	private PersistenciaSuperAndes ()
	{
		pmf = JDOHelper.getPersistenceManagerFactory("SuperAndes");		
		crearClasesSQL ();
		
		// Define los nombres por defecto de las tablas de la base de datos
		tablas = new LinkedList<String> ();
		tablas.add ("Superandes_sequence");
		tablas.add ("BODEGA_INSUMO");
		tablas.add ("SUCURSAL");
		tablas.add ("ESTANTE");
		tablas.add ("BODEGA");
		tablas.add ("PROVEEDOR");
		tablas.add ("PRODUCTO");
		tablas.add ("INSUMO");
		tablas.add ("CONSUMIDOR");
		tablas.add ("PROMOCION");
		tablas.add ("PEDIDO");
		tablas.add ("VENTA");
		tablas.add("CARRITO");
		tablas.add("CARRITO_INSUMO");
}

	/**
	 * Constructor privado, que recibe los nombres de las tablas en un objeto Json - Patrón SINGLETON
	 * @param tableConfig - Objeto Json que contiene los nombres de las tablas y de la unidad de persistencia a manejar
	 */
	private PersistenciaSuperAndes (JsonObject tableConfig)
	{
		crearClasesSQL ();
		tablas = leerNombresTablas (tableConfig);

		String unidadPersistencia = tableConfig.get ("unidadPersistencia").getAsString ();
		log.trace ("Accediendo unidad de persistencia: " + unidadPersistencia);
		pmf = JDOHelper.getPersistenceManagerFactory (unidadPersistencia);
	}

	/**
	 * @return Retorna el único objeto PersistenciaSuperAndes existente - Patrón SINGLETON
	 */
	public static PersistenciaSuperAndes getInstance ()
	{
		if (instance == null)
		{
			instance = new PersistenciaSuperAndes ();
		}
		return instance;
	}
	
	/**
	 * Constructor que toma los nombres de las tablas de la base de datos del objeto tableConfig
	 * @param tableConfig - El objeto JSON con los nombres de las tablas
	 * @return Retorna el único objeto PersistenciaParranderos existente - Patrón SINGLETON
	 */
	public static PersistenciaSuperAndes getInstance (JsonObject tableConfig)
	{
		if (instance == null)
		{
			instance = new PersistenciaSuperAndes (tableConfig);
		}
		return instance;
	}

	/**
	 * Cierra la conexión con la base de datos
	 */
	public void cerrarUnidadPersistencia ()
	{
		pmf.close ();
		instance = null;
	}
	
	/**
	 * Genera una lista con los nombres de las tablas de la base de datos
	 * @param tableConfig - El objeto Json con los nombres de las tablas
	 * @return La lista con los nombres del secuenciador y de las tablas
	 */
	private List <String> leerNombresTablas (JsonObject tableConfig)
	{
		JsonArray nombres = tableConfig.getAsJsonArray("tablas") ;

		List <String> resp = new LinkedList <String> ();
		for (JsonElement nom : nombres)
		{
			resp.add (nom.getAsString ());
		}
		
		return resp;
	}
	
	/**
	 * Crea los atributos de clases de apoyo SQL
	 */
	private void crearClasesSQL ()
	{
		sqlBodega_Insumo = new SQLBodega_Insumo(this);
		sqlBodega = new SQLBodega(this);
		sqlEstante = new SQLEstante(this);
		sqlInsumo = new SQLInsumo(this);
		sqlPedido = new SQLPedido(this);
		sqlConsumidor = new SQLConsumidor(this);
		sqlProducto = new SQLProducto(this);	
		sqlPromocion = new SQLPromocion(this);
		sqlProveedor = new SQLProveedor(this);
		sqlSucursal = new SQLSucursal(this);
		sqlCarritoInsumo = new SQLCarrito_Insum(this) ;
		sqlVenta = new SQLVenta(this);
		sqlUtil = new SQLUtil(this);
	}

	/**
	 * @return La cadena de caracteres con el nombre del secuenciador de parranderos
	 */
	public String darSeqSuperAndes ()
	{
		return tablas.get (0);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla Bodega_insumo de SuperAndes
	 */
	public String darTablaBod_Insumo ()
	{
		return tablas.get (1);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Visitan de parranderos
	 */
	public String darTablaSucursal()
	{
		return tablas.get (2);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Bar de parranderos
	 */
	public String darTablaEstante ()
	{
		return tablas.get (3);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de TipoBebida de parranderos
	 */
	public String darTablaBodega ()
	{
		return tablas.get (4);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Visitan de parranderos
	 */
	public String darTablaProveedor()
	{
		return tablas.get (5);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Visitan de parranderos
	 */
	public String darTablaProducto ()
	{
		return tablas.get (6);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Bebedor de parranderos
	 */
	public String darTablaInsumo()
	{
		return tablas.get (7);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla Consumidor de SuperAndes
	 */
	public String darSeqConsumidor ()
	{
		return tablas.get (8);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Visitan de parranderos
	 */
	public String darTablaPromocion ()
	{
		return tablas.get (9);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Gustan de parranderos
	 */
	public String darTablaPedido ()
	{
		return tablas.get (10);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Visitan de parranderos
	 */
	public String darTablaVenta()
	{
		return tablas.get (11);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Visitan de parranderos
	 */
	public String darTablaCarrito ()
	{
		return tablas.get (12);
	}

	
	public String darTablaCarritoInsum()
	{
		return tablas.get(13) ;
	}
	

	/**
	 * Transacción para el generador de secuencia de SuperAndes
	 * Adiciona entradas al log de la aplicación
	 * @return El siguiente número del secuenciador de Parranderos
	 */
	private long nextval ()
	{
        long resp = sqlUtil.nextval (pmf.getPersistenceManager());
        log.trace ("Generando secuencia: " + resp);
        return resp;
    }
	
	/**
	 * Extrae el mensaje de la exception JDODataStoreException embebido en la Exception e, que da el detalle específico del problema encontrado
	 * @param e - La excepción que ocurrio
	 * @return El mensaje de la excepción JDO
	 */
	private String darDetalleException(Exception e) 
	{
		String resp = "";
		if (e.getClass().getName().equals("javax.jdo.JDODataStoreException"))
		{
			JDODataStoreException je = (javax.jdo.JDODataStoreException) e;
			return je.getNestedExceptions() [0].getMessage();
		}
		return resp;
	}

	/* ****************************************************************
	 * 			Métodos para manejar las BODEGAS
	 *****************************************************************/

	/**
	 * Método que consulta todas las tuplas en la tabla TipoBebida
	 * @return La lista de objetos TipoBebida, construidos con base en las tuplas de la tabla TIPOBEBIDA
	 */
	public List<Bodega> darBodegas()
	{
		return sqlBodega.darBodegas(pmf.getPersistenceManager());
	}

	/* ****************************************************************
	 * 			Métodos para manejar los ESTANTES
	 *****************************************************************/
	
	/**
	 * Método que consulta todas las tuplas en la tabla BEBEDOR
	 * @return La lista de objetos BEBEDOR, construidos con base en las tuplas de la tabla BEBEDOR
	 */
	public List<Estante> darEstantes ()
	{
		return sqlEstante.darEstantes (pmf.getPersistenceManager());
	}
	
	/* ****************************************************************
	 * 			Métodos para manejar los INSUMOS
	 *****************************************************************/
	
	/**
	 * Método que consulta todas las tuplas en la tabla BAR
	 * @return La lista de objetos BAR, construidos con base en las tuplas de la tabla BAR
	 */
	public List<Insumo> darInsumos ()
	{
		return sqlInsumo.darInsumos (pmf.getPersistenceManager());
	}
	
	/* ****************************************************************
	 * 			Métodos para manejar la relación PEDIDO
	 *****************************************************************/
	
	/**
	 * Método que consulta todas las tuplas en la tabla GUSTAN
	 * @return La lista de objetos GUSTAN, construidos con base en las tuplas de la tabla GUSTAN
	 */
	public List<Pedido> darPedidos()
	{
		return sqlPedido.darPedidos (pmf.getPersistenceManager());
	}

	/* ****************************************************************
	 * 			Métodos para manejar los PRODUCTOS
	 *****************************************************************/

	/**
	 * Método que consulta todas las tuplas en la tabla VISITAN
	 * @return La lista de objetos VISITAN, construidos con base en las tuplas de la tabla VISITAN
	 */
	public List<Producto> darProductos ()
	{
		return sqlProducto.darProductos (pmf.getPersistenceManager());
	}	
	
	/* ****************************************************************
	 * 			Métodos para manejar las PROMOCIONES
	 *****************************************************************/

	/**
	 * Método que consulta todas las tuplas en la tabla VISITAN
	 * @return La lista de objetos VISITAN, construidos con base en las tuplas de la tabla VISITAN
	 */
	public List<Promocion> darPromociones ()
	{
		return sqlPromocion.darPromociones (pmf.getPersistenceManager());
	}
	
	/* ****************************************************************
	 * 			Métodos para manejar los PRODUCTOS
	 *****************************************************************/

	/**
	 * Método que consulta todas las tuplas en la tabla VISITAN
	 * @return La lista de objetos VISITAN, construidos con base en las tuplas de la tabla VISITAN
	 */
	public List<Proveedor> darProveedores ()
	{
		return sqlProveedor.darProveedores (pmf.getPersistenceManager());
	}
	
	/* ****************************************************************
	 * 			Métodos para manejar las SUCURSALES
	 *****************************************************************/

	/**
	 * Método que consulta todas las tuplas en la tabla VISITAN
	 * @return La lista de objetos VISITAN, construidos con base en las tuplas de la tabla VISITAN
	 */
	public List<Sucursal> darSucursales ()
	{
		return sqlSucursal.darSucursales (pmf.getPersistenceManager());
	}


	/* ****************************************************************
	 * 			Métodos para manejar las VENTAS_EMP
	 *****************************************************************/

	/**
	 * Método que consulta todas las tuplas en la tabla VENTA
	 * @return La lista de objetos VENTA, construidos con base en las tuplas de la tabla VENTA
	 */
	public List<Venta> darVentas ()
	{
		return sqlVenta.darVentas(pmf.getPersistenceManager());
	}

	/* ****************************************************************
	 * 			Métodos para manejar los PROVEEDORES
	 *****************************************************************/

	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROVEEDOR
	 * Adiciona entradas al log de la aplicación
	 * @param nombre - El nombre del proveedors
	 * @return El objeto Proveedor adicionado. null si ocurre alguna Excepción
	 */
	public Proveedor registrarProveedor(String nombre)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			int nit = (int)nextval();
			long tuplasInsertadas = sqlProveedor.registrarProveedor(pm, nit, nombre);
			tx.commit();

			log.trace ("Inserción de tipo proveedor: " + nombre + ": " + tuplasInsertadas + " tuplas insertadas");

			ArrayList<Producto> lista = new ArrayList<Producto>( );
			return new Proveedor(nit, nombre, lista);
		}
		catch (Exception e)
		{
//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* ****************************************************************
	 * 			Métodos para manejar los PRODUCTOS
	 *****************************************************************/

	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PRODUCTO
	 * Adiciona entradas al log de la aplicación
	 * @param nombre - El nombre del producto
	 * @param precio - El precio del producto
	 * @param marca - La marca del producto
	 * @param calificacionCalidad - La clasificación de calodad del producto
	 * @param presentacion - La presentación del producto
	 * @param cantidadPresentacion - La cantidad por presentación del producto
	 * @param clasificacion - La clasificación del producto
	 * @param precioPorUnidadDeMedida - El precio por unidad de medida del producto
	 * @param tipoProducto - El tipo del producto
	 * @param unidadDeMedida - La unidad de medida del producto
	 * @return El objeto Producto adicionado. null si ocurre alguna Excepción
	 */
	public Producto adicionarProducto(String nombre, int precio, String marca,
									  double calificacionCalidad, String presentacion,
									  int cantidadPresentacion, String clasificacion,
									  int precioPorUnidadDeMedida, String tipoProducto,
									  String unidadDeMedida)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			int codigoDeBarras = (int)nextval();
			long tuplasInsertadas = sqlProducto.adicionarProducto(pm, codigoDeBarras, nombre,
																	precio, marca, calificacionCalidad,
																	presentacion, cantidadPresentacion,
																	clasificacion, precioPorUnidadDeMedida,
																	tipoProducto, unidadDeMedida);
			tx.commit();

			log.trace ("Inserción de tipo producto: " + nombre + ": " + tuplasInsertadas + " tuplas insertadas");

			ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();

			return new Producto(codigoDeBarras, nombre, precio, marca, calificacionCalidad,
			presentacion, cantidadPresentacion, clasificacion, precioPorUnidadDeMedida,
			tipoProducto, unidadDeMedida, proveedores);
		}
		catch (Exception e)
		{
//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* ****************************************************************
	 * 			Métodos para manejar los CONSUMIDORES
	 *****************************************************************/

	/**
	 * Método que consulta todas las tuplas en la tabla CONSUMIDOR
	 * @return La lista de objetos CONSUMIDOR, construidos con base en las tuplas de la tabla CONSUMIDOR
	 */
	public List<Consumidor> darConsumidores ()
	{
		return sqlConsumidor.darConsumidores(pmf.getPersistenceManager());
	}

	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla CONSUMIDOR
	 * Adiciona entradas al log de la aplicación
	 * @param nombre - El nombre del consumidor
	 * @param direccion - La dirección del consumidor
	 * @param correo - El correo del consumidor
	 * @param puntosAcumulados - Los puntos acumulados del consumidor
	 * @param tipo - El tipo del consumidor
	 * @return El objeto Producto adicionado. null si ocurre alguna Excepción
	 */
	public Consumidor registrarConsumidor(String nombre, String direccion, String correo, int puntosAcumulados, String tipo)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			int identificacion = (int)nextval();
			long tuplasInsertadas = sqlConsumidor.registrarConsumidor(pm, identificacion, nombre, direccion, correo, puntosAcumulados, tipo);
			tx.commit();

			log.trace ("Inserción de tipo consumidor: " + nombre + ": " + tuplasInsertadas + " tuplas insertadas");

			ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();

			return new Consumidor(identificacion, correo, nombre, puntosAcumulados, direccion, tipo);
		}
		catch (Exception e)
		{
//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* ****************************************************************
	 * 			Métodos para manejar las SUCURSALES
	 *****************************************************************/

	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla SUCURSAL
	 * Adiciona entradas al log de la aplicación
	 * @param nombre - El nombre de la sucursal
	 * @param ciudad - La ciudad de la sucursal
	 * @param direccion - La dirección de la sucursal
	 * @return El objeto Producto adicionado. null si ocurre alguna Excepción
	 */
	public Sucursal registrarSucursal(String nombre, String ciudad, String direccion)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			int id = (int)nextval();
			long tuplasInsertadas = sqlSucursal.registrarSucursal(pm, id, nombre, ciudad, direccion);
			tx.commit();

			log.trace ("Inserción de tipo sucursal: " + nombre + ": " + tuplasInsertadas + " tuplas insertadas");

			ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();
			return new Sucursal(nombre, direccion, ciudad);
		}
		catch (Exception e)
		{
//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* ****************************************************************
	 * 			Métodos para manejar las BODEGAS
	 *****************************************************************/

	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla SUCURSAL
	 * @param idSucursal - El identificador de la sucursal a la cual pertenece la bodega
	 * @param cantidadActual - La cantidad de productos que hay en la blodega
	 * @param capacidadPeso - La capasidad en peso de la bodega
	 * @param capacidadVolumen - La capasidad en volumen de la bodega
	 * @param tipoProducto - El tipo de producto de la bodega
	 * @return El objeto Producto adicionado. null si ocurre alguna Excepción
	 */
	public Bodega registrarBodega(int idSucursal, int cantidadActual, int capacidadPeso, int capacidadVolumen, String tipoProducto)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try
		{
			tx.begin();
			int id = (int)nextval();
			long tuplasInsertadas = sqlBodega.registrarBodega(pm, idSucursal, id, cantidadActual, capacidadPeso, capacidadVolumen, tipoProducto);
			tx.commit();

			log.trace ("Inserción de tipo bodega: " + id + ": " + tuplasInsertadas + " tuplas insertadas");

			return new Bodega(cantidadActual, capacidadPeso, capacidadVolumen, tipoProducto);
		}
		catch (Exception e)
		{
//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que consulta todas las tuplas en la tabla BODEGA que son del tipo dado
	 * @param tipo - El tipo de la bodega
	 * @return El id de la bodega
	 */
	public int darBodegaPorTipo (String tipo)
	{
		return sqlBodega.darBodegaPorTipo(pmf.getPersistenceManager(), tipo);
	}

	/* ****************************************************************
	 * 			Métodos para manejar los ESTANTES
	 *****************************************************************/

	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla SUCURSAL
	 * @param idSucursal - El identificador de la sucursal a la cual pertenece el estante
	 * @param cantidadActual - La cantidad de productos que hay en la blodega
	 * @param capacidadPeso - La capasidad en peso de la bodega
	 * @param capacidadVolumen - La capasidad en volumen de la bodega
	 * @param nivelAbastecimiento -  El nivel de abastecimiento del estante
	 * @param tipoProducto - El tipo de producto de la bodega
	 * @return El objeto Producto adicionado. null si ocurre alguna Excepción
	 */
	public Estante registrarEstante(int idSucursal, int cantidadActual, int capacidadPeso, int capacidadVolumen, int nivelAbastecimiento, String tipoProducto)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try
		{
			tx.begin();
			int id = (int)nextval();
			long tuplasInsertadas = sqlEstante.registrarEstante(pm, idSucursal, id, cantidadActual, capacidadPeso, capacidadVolumen, nivelAbastecimiento, tipoProducto);
			tx.commit();

			log.trace ("Inserción de tipo estante: " + id + ": " + tuplasInsertadas + " tuplas insertadas");

			return new Estante(cantidadActual, capacidadPeso, capacidadVolumen, nivelAbastecimiento, tipoProducto);
		}
		catch (Exception e)
		{
//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* ****************************************************************
	 * 			Métodos para manejar las PROMOCIONES
	 *****************************************************************/

	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROMOCION
	 * @param idSucursal - El identificador de la sucursal a la cual pertenece la promoción
	 * @param nombre - El nombre de la promoción
	 * @param estado - El estado de la promoción
	 * @param fechaInicio - La fecha de inicio de la promoción
	 * @param fechaFinal - La fecha de fin de la promoción
	 * @param especificaciones - Las especificiaciones de la promoción
	 * @param tipoPromocion - El tipo de la promoción
	 * @param cantidadVendidos - La cantidad de productos vendidos
	 * @param insumo - El insumo de la promoción
	 * @return El objeto Producto adicionado. null si ocurre alguna Excepción
	 */
	public Promocion registrarPromocion(int idSucursal, String nombre, String estado,
										Date fechaInicio, Date fechaFinal, String especificaciones, int tipoPromocion,
										int insumo, int cantidadVendidos)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try
		{
			tx.begin();
			int id = (int)nextval();
			long tuplasInsertadas = sqlPromocion.registrarPromocion(pm, idSucursal, id, nombre, estado,
																	fechaInicio, fechaFinal, especificaciones,
																	tipoPromocion, insumo, cantidadVendidos);
			tx.commit();

			log.trace ("Inserción de tipo promoción: " + id + ": " + tuplasInsertadas + " tuplas insertadas");

			Insumo pInsumo = sqlInsumo.darInsumoPorId(pm, insumo);
			return new Promocion(id, nombre	, estado, fechaInicio, fechaFinal, especificaciones, tipoPromocion, pInsumo);
		}
		catch (Exception e)
		{
//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla PROMOCION, dado el identificador de la promoción
	 * Adiciona entradas al log de la aplicación
	 * @param idPromocion - El identificador de la promoción
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public int eliminarPromocionPorId(int idPromocion)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			int tuplasEliminadas = sqlPromocion.eliminarPromocionPorId(pm, idPromocion);
			tx.commit();

			log.trace ("Eliminación por id de tipo promoción: " + idPromocion + ": " + tuplasEliminadas + " tuplas eliminadas");

			return tuplasEliminadas;
		}
		catch (Exception e)
		{
//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return -1;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* ****************************************************************
	 * 			Métodos para manejar los INSUMOS
	 *****************************************************************/

	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla BODEGA_INSUMO
	 * @param idBodega - El identificador de la bodega
	 * @param idInsumo - El identificador del insumo
	 * @return True si se agrega la tupla. false si ocurre alguna Excepción
	 */
	public boolean registrarBodegaInsumo(int idBodega, int idInsumo)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try
		{
			tx.begin();
			long tuplasInsertadas = sqlBodega_Insumo.registrarBodegaInsumo(pm, idBodega, idInsumo);
			tx.commit();

			log.trace ("Inserción de tipo bodega_insumo: IdBodega: " + idBodega + ", IdInsumo: " + idInsumo + ". " + tuplasInsertadas + " tuplas insertadas");

			return true;
		}
		catch (Exception e)
		{
//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return false;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que consulta todas las tuplas en la tabla INSUMO que tienen el identificador dado
	 * @param idInsumo - El identificador del insumo
	 * @return El objeto INSUMO, construido con base en la tuplas de la tabla INSUMO, que tiene el identificador dado
	 */
	public Insumo darInsumoPorId (int idInsumo)
	{
		return sqlInsumo.darInsumoPorId (pmf.getPersistenceManager(), idInsumo);
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla INSUMO, dado el identificador del insumo
	 * Adiciona entradas al log de la aplicación
	 * @param idInsumo - El identificador de la promoción
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public int eliminarInsumoPorId(int idInsumo)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			int tuplasEliminadas = sqlInsumo.eliminarInsumoPorId(pm, idInsumo);
			tx.commit();

			log.trace ("Eliminación por id de tipo insumo: " + idInsumo + ": " + tuplasEliminadas + " tuplas eliminadas");

			return tuplasEliminadas;
		}
		catch (Exception e)
		{
//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return -1;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* ****************************************************************
	 * 			Métodos para manejar las VENTAS
	 *****************************************************************/

	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla VENTA
	 * @param idSucursal - El identificador de la sucursal a la cual pertenece la venta
	 * @param costoVenta - El costo total de la venta
	 * @param estado - El estado de la venta
	 * @param puntos - Los puntos de la venta
	 * @param idConsumidor - El id del consumidor
	 * @param fecha - fecha de la venta
	 * @return El objeto Venta adicionado. null si ocurre alguna Excepción
	 */
	public Venta registrarVenta(int idSucursal, int idConsumidor, int costoVenta,
								  String estado, int puntos, java.util.Date fecha)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try
		{
			tx.begin();
			int id = (int)nextval();
			long tuplasInsertadas = sqlVenta.registrarVenta(pm, idSucursal, id, idConsumidor, costoVenta, estado, puntos, fecha);
			tx.commit();

			log.trace ("Inserción de tipo venta en la sucursal: " + idSucursal + ", Venta: " + id + ". " + tuplasInsertadas + " tuplas insertadas");

			Venta venta = new Venta(id, costoVenta, estado, fecha, puntos);
			return venta;
		}
		catch (Exception e)
		{
//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* ****************************************************************
	 * 			Métodos para manejar los CARRITOS
	 *****************************************************************/

	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla VENTA
	 * @param idConsumidor - El identificador del consumidor
	 * @return True si se agrega la tupla. false si ocurre alguna Excepción
	 */
	public boolean registrarCarrito(int idConsumidor)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try
		{
			tx.begin();
			int id = (int)nextval();
			long tuplasInsertadas = sqlCarrito.registrarCarrito(pm, id, idConsumidor);
			tx.commit();

			log.trace ("Inserción de tipo carrito: " + id + ", Consumidor: " + idConsumidor + ". " + tuplasInsertadas + " tuplas insertadas");

			return true;
		}
		catch (Exception e)
		{
//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return false;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Elimina todas las tuplas de todas las tablas de la base de datos de Parranderos
	 * Crea y ejecuta las sentencias SQL para cada tabla de la base de datos - EL ORDEN ES IMPORTANTE 
	 * @return Un arreglo con 7 números que indican el número de tuplas borradas en las tablas GUSTAN, SIRVEN, VISITAN, BEBIDA,
	 * TIPOBEBIDA, BEBEDOR y BAR, respectivamente
	 */
	public long [] limpiarSuperAndes ()
	{
		PersistenceManager pm = pmf.getPersistenceManager();
        Transaction tx=pm.currentTransaction();
        try
        {
            tx.begin();
            long [] resp = sqlUtil.limpiarSuperAndes (pm);
            tx.commit ();
            log.info ("Borrada la base de datos");
            return resp;
        }
        catch (Exception e)
        {
        	// e.printStackTrace();
        	log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
        	return new long[] {-1, -1, -1, -1, -1, -1, -1};
        }
        finally
        {
            if (tx.isActive())
            {
                tx.rollback();
            }
            pm.close();
        }
		
	}
	
	
	
	
	public boolean adicionarProductoACarrito(int idCarrito, int codigoBarrasInsumo)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try
		{
			tx.begin();
			
			long tuplasInsertadas = sqlCarritoInsumo.adicionarProductoACarrito(pm, idCarrito, codigoBarrasInsumo);
			tx.commit();

			return true;
		}
		catch (Exception e)
		{
//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return false;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	
	
	
	public boolean eliminarProductoACarrito( int idCarrito, int codigoBarrasInsumo)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			int tuplasEliminadas = sqlCarritoInsumo.eliminarProductoACarrito(pm, idCarrito, codigoBarrasInsumo);
			tx.commit();

			return true;
		}
		catch (Exception e)
		{
//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return false;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	
	

}
