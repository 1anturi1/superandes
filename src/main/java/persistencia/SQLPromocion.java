package persistencia;

import java.sql.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import negocio.Promocion;

public class SQLPromocion {

	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes pp;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLPromocion (PersistenciaSuperAndes pp)
	{
		this.pp = pp;
	}
	

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de BODEGAS de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos BODEGA
	 */
	public List<Promocion> darPromociones(PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + pp.darTablaPromocion());
		q.setResultClass(Promocion.class);
		return (List<Promocion>) q.executeList();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para adicionar una PROMOCION a la base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @param idSucursal - El identificador de la sucursal a la cual pertenece la promoción
	 * @param id - El identificador de la promocion
	 * @param nombre - El nombre de la promoción
	 * @param estado - El estado de la promoción
	 * @param fechaInicio - La fecha de inicio de la promoción
	 * @param fechaFinal - La fecha de fin de la promoción
	 * @param especificaciones - Las especificiaciones de la promoción
	 * @param tipoPromocion - El tipo de la promoción
	 * @param cantidadVendidos - La cantidad de productos vendidos
	 * @param idInsumo - El insumo de la promoción
	 * @return EL número de tuplas insertadas
	 */
	public long registrarPromocion(PersistenceManager pm, int idSucursal, int id, String nombre, String estado,
								   Date fechaInicio, Date fechaFinal, String especificaciones, int tipoPromocion,
								   int idInsumo, int cantidadVendidos)
	{
		String pQuery = "(id, estado, nombre, especificaciones, tipo, fecha_ini, fecha_fin, insumo, cant_productos, id_sucursal) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaProducto () + pQuery);
		q.setParameters(id, estado, nombre, especificaciones, tipoPromocion, fechaInicio, fechaFinal, idInsumo, cantidadVendidos, idSucursal);
		return (long) q.executeUnique();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para eliminar UNA PROMOCION de la base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @param idPromocion - El identificador de la promoción
	 * @return EL número de tuplas eliminadas
	 */
	public int eliminarPromocionPorId(PersistenceManager pm, int idPromocion)
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + pp.darTablaPromocion() + " WHERE id = ?");
		q.setParameters(idPromocion);
		return (int) q.executeUnique();
	}
}
