package persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLBodega_Insumo {
    /* ****************************************************************
     * 			Constantes
     *****************************************************************/
    /**
     * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
     * Se renombra acá para facilitar la escritura de las sentencias
     */
    private final static String SQL = PersistenciaSuperAndes.SQL;

    /* ****************************************************************
     * 			Atributos
     *****************************************************************/
    /**
     * El manejador de persistencia general de la aplicación
     */
    private PersistenciaSuperAndes pp;

    /* ****************************************************************
     * 			Métodos
     *****************************************************************/

    /**
     * Constructor
     * @param pp - El Manejador de persistencia de la aplicación
     */
    public SQLBodega_Insumo (PersistenciaSuperAndes pp)
    {
        this.pp = pp;
    }

    /**
     * Crea y ejecuta la sentencia SQL para adicionar una BODEGA_INSUMO a la base de datos de SuperAndes
     * @param pm - El manejador de persistencia
     * @param idBodega - El identificador de la bodega
     * @param idInsumo - El identificador del insumo
     * @return EL número de tuplas insertadas
     */
    public long registrarBodegaInsumo(PersistenceManager pm, int idBodega, int idInsumo)
    {
        String pQuery = "(codigo_barras_ins, id_bod) values (?, ?)";
        Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaBod_Insumo () + pQuery);
        q.setParameters(idInsumo, idBodega);
        return (long) q.executeUnique();
    }
}
