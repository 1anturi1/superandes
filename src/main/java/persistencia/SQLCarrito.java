package persistencia;

import negocio.Carrito;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.util.List;

public class SQLCarrito
{
    /* ****************************************************************
     * 			Constantes
     *****************************************************************/
    /**
     * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
     * Se renombra acá para facilitar la escritura de las sentencias
     */
    private final static String SQL = PersistenciaSuperAndes.SQL;

    /* ****************************************************************
     * 			Atributos
     *****************************************************************/
    /**
     * El manejador de persistencia general de la aplicación
     */
    private PersistenciaSuperAndes pp;

    /* ****************************************************************
     * 			Métodos
     *****************************************************************/

    /**
     * Constructor
     * @param pp - El Manejador de persistencia de la aplicación
     */
    public SQLCarrito (PersistenciaSuperAndes pp)
    {
        this.pp = pp;
    }

    /**
     * Crea y ejecuta la sentencia SQL para encontrar la información de BODEGAS de la
     * base de datos de SuperAndes
     * @param pm - El manejador de persistencia
     * @return Una lista de objetos BODEGA
     */
    public List<Carrito> darCarritos(PersistenceManager pm)
    {
        Query q = pm.newQuery(SQL, "SELECT * FROM " + pp.darTablaPromocion());
        q.setResultClass(Carrito.class);
        return (List<Carrito>) q.executeList();
    }

    /**
     * Crea y ejecuta la sentencia SQL para encontrar la información de un consumidor de la
     * base de datos de SuperAndes, por su identificador
     * @param pm - El manejador de persistencia
     * @param idCarrito - El identificador del insumo
     * @return El objeto INSUMO que tiene el identificador dado
     */
    public int darConsumidorPorIdCarrito (PersistenceManager pm, int idCarrito)
    {
        Query q = pm.newQuery(SQL, "SELECT * FROM " + pp.darTablaCarrito () + " WHERE id = ?");
        q.setParameters(idCarrito);
        return (int) q.executeUnique();
    }

    /**
     * Crea y ejecuta la sentencia SQL para adicionar una CARRITO a la base de datos de SuperAndes
     * @param pm - El manejador de persistencia
     * @param id - El identificador del carrito
     * @param idConsumidor - El identificador del consumidor
     * @return EL número de tuplas insertadas
     */
    public long registrarCarrito(PersistenceManager pm, int id, int idConsumidor)
    {
        String pQuery = "(id, id_consum) values (?, ?)";
        Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaCarrito () + pQuery);
        q.setParameters(id, idConsumidor);
        return (long) q.executeUnique();
    }
    
    public long abandonarCarrito(PersistenceManager pm, int id_client)
    {
		Query q = pm.newQuery(SQL, "DELETE FROM " + pp.darTablaCarrito() + " WHERE ID_CONSUM = ? ");
		q.setParameters(id_client);
		return (int) q.executeUnique();
    }
    
    
    
    
}
