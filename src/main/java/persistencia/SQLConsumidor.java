package persistencia;

import negocio.Consumidor;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.util.List;

public class SQLConsumidor
{

    /* ****************************************************************
     * 			Constantes
     *****************************************************************/
    /**
     * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
     * Se renombra acá para facilitar la escritura de las sentencias
     */
    private final static String SQL = PersistenciaSuperAndes.SQL;

    /* ****************************************************************
     * 			Atributos
     *****************************************************************/
    /**
     * El manejador de persistencia general de la aplicación
     */
    private PersistenciaSuperAndes pp;

    /* ****************************************************************
     * 			Métodos
     *****************************************************************/

    /**
     * Constructor
     * @param pp - El Manejador de persistencia de la aplicación
     */
    public SQLConsumidor (PersistenciaSuperAndes pp)
    {
        this.pp = pp;
    }

    /**
     * Crea y ejecuta la sentencia SQL para adicionar un CONSUMIDOR a la base de datos de SuperAndes
     * @param pm - El manejador de persistencia
     * @param identificacion - La identificación del consumidor
     * @param nombre - El nombre del consumidor
     * @param direccion - La dirección del consumidor
     * @param correo - El correo del consumidor
     * @param puntosAcumulados - Los puntos acumulados del consumidor
     * @param tipo - El tipo del consumidor
     * @return EL número de tuplas insertadas
     */
    public long registrarConsumidor(PersistenceManager pm, int identificacion, String nombre,
                                    String direccion, String correo, int puntosAcumulados, String tipo)
    {
        String pQuery = "(tipo, nit, nombre, correo, puntos, direccion) values (?, ?, ?, ?, ?, ?)";
        Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaProveedor () + pQuery);
        q.setParameters(tipo, identificacion, nombre, correo, puntosAcumulados, direccion);
        return (Long) q.executeUnique();
    }

    public void aumentarPuntos(PersistenceManager pm, int id, int puntos)
    {
        String pQuery = "SET puntos = puntos + " + puntos + " WHERE nit = " + id ;
        Query q = pm.newQuery(SQL, "UPDATE " + pp.darTablaConsumidor() + pQuery);
        q.setParameters(puntos);
    }
    /**
     * Crea y ejecuta la sentencia SQL para encontrar la información de CONSUMIDOR de la
     * base de datos de SuperAndes
     * @param pm - El manejador de persistencia
     * @return Una lista de objetos CONSUMIDOR
     */
    public List<Consumidor> darConsumidores(PersistenceManager pm)
    {
        Query q = pm.newQuery(SQL, "SELECT * FROM " + pp.darTablaBodega ());
        q.setResultClass(Consumidor.class);
        return (List<Consumidor>) q.executeList();
    }
}
