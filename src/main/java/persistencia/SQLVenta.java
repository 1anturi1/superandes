package persistencia;

import java.util.List;
import java.util.Date;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import negocio.Venta;

public class SQLVenta {
	
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes pp;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLVenta(PersistenciaSuperAndes pp)
	{
		this.pp = pp;
	}

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de BODEGAS de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos BODEGA
	 */
	public List<Venta> darVentas(PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + pp.darTablaVenta());
		q.setResultClass(Venta.class);
		return (List<Venta>) q.executeList();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para adicionar una VENTA a la base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @param idSucursal - El identificador de la sucursal a la cual pertenece la venta
	 * @param id - El identificador de la venta
	 * @param costoVenta - El costo total de la venta
	 * @param estado - El estado de la venta
	 * @param puntos - Los puntos de la venta
	 * @param idConsumidor - El id del consumidor
	 * @param fecha - fecha de la venta
	 * @return EL número de tuplas insertadas
	 */
	public long registrarVenta(PersistenceManager pm, int idSucursal, int id, int idConsumidor, int costoVenta,
							   String estado, int puntos, Date fecha)
	{
		String pQuery = "(id, costo_total, estadp, puntos, id_consum, fecha, id_sucursal) values (?, ?, ?, ?, ?, ?, ?)";
		Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaProducto () + pQuery);
		q.setParameters(id, costoVenta, estado, puntos, idConsumidor, fecha, idSucursal);
		return (long) q.executeUnique();
	}
}
