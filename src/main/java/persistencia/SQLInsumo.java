package persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import negocio.Bodega;
import negocio.Estante;
import negocio.Insumo;

public class SQLInsumo {

	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes pp;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLInsumo (PersistenciaSuperAndes pp)
	{
		this.pp = pp;
	}

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de UN ISUMO de la
	 * base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @param idInsumo - El identificador del insumo
	 * @return El objeto INSUMO que tiene el identificador dado
	 */
	public Insumo darInsumoPorId (PersistenceManager pm, int idInsumo)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + pp.darTablaInsumo () + " WHERE codigo_barras = ?");
		q.setResultClass(Insumo.class);
		q.setParameters(idInsumo);
		return (Insumo) q.executeUnique();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de BODEGAS de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos BODEGA
	 */
	public List<Insumo> darInsumos(PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + pp.darTablaInsumo ());
		q.setResultClass(Insumo.class);
		return (List<Insumo>) q.executeList();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para eliminar UNA INSUMO de la base de datos de SuperAndes, por su insumo
	 * @param pm - El manejador de persistencia
	 * @param idInsumo - El identificador de la promoción
	 * @return EL número de tuplas eliminadas
	 */
	public int eliminarInsumoPorId(PersistenceManager pm, int idInsumo)
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + pp.darTablaInsumo() + " WHERE id = ?");
		q.setParameters(idInsumo);
		return (int) q.executeUnique();
	}
}
