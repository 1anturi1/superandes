package negocio;

public class Pedido implements VOPedido
{
	//---------------------------------------------------------------
	//ATRIBUTOS
	//---------------------------------------------------------------


	private int cantidadProductos ;

	private int costo ;

	private String estado ;

	private int producto ;

	//---------------------------------------------------------------
	//METODOS
	//---------------------------------------------------------------

	/**
	 * constructor por default
	 */
	public Pedido()
	{
		cantidadProductos = 0 ;
		costo = 0 ;
		estado = "" ;
		producto = 0 ;
	}

	/**
	 * @param cantidadProductos
	 * @param costo
	 * @param estado
	 * @param producto
	 */
	public Pedido(int cantidadProductos, int costo, String estado, int producto) {

		this.cantidadProductos = cantidadProductos;
		this.costo = costo;
		this.estado = estado;
		this.producto = producto;
	}

	/**
	 * @return the cantidadProductos
	 */
	public int getCantidadProductos() {
		return cantidadProductos;
	}

	/**
	 * @param cantidadProductos the cantidadProductos to set
	 */
	public void setCantidadProductos(int cantidadProductos) {
		this.cantidadProductos = cantidadProductos;
	}

	/**
	 * @return the costo
	 */
	public int getCosto() {
		return costo;
	}

	/**
	 * @param costo the costo to set
	 */
	public void setCosto(int costo) {
		this.costo = costo;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the producto
	 */
	public int getProducto() {
		return producto;
	}

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(int producto) {
		this.producto = producto;
	}

	@Override
	/**
	 * @return 
	 */
	public String toString()
	{
		return "" ;
	}








}
