package negocio;

import java.util.List;

public interface VOCarrito
{
    /**
     * @return the prodCarrito
     */
    public List<Insumo> getProdCarrito();

    /**
     * @return the precio
     */
    public int getPrecio();
}
