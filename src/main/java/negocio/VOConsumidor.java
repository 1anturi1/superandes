package negocio;

public interface VOConsumidor
{
    /**
     * @return the identificacion
     */
    public int getIdentificacion() ;

    /**
     * @return the direccion
     */
    public String getDireccion() ;

    /**
     * @return the tipo
     */
    public String getTipo() ;

    /**
     * @return the correo
     */
    public String getCorreo() ;

    /**
     * @return the nombre
     */
    public String getNombre() ;

    /**
     * @return the puntosAcumulados
     */
    public int getPuntosAcumulados() ;

    @Override
    public String toString() ;
}
