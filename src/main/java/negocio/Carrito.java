package negocio;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Carrito implements VOCarrito
{

	//---------------------------------------------------------------
	//ATRIBUTOS
	//---------------------------------------------------------------

	/*
	 * Productos agregados en el carrito
	 */
	private List<Insumo> prodCarrito ;

	/*
	 * precio total de los producto agregados en el carrito
	 */
	private int precio ;

	//---------------------------------------------------------------
	//METODOS
	//---------------------------------------------------------------


	/**
	 *Constructor por defecto  
	 */

	public Carrito()
	{
		prodCarrito = new ArrayList<>() ;
		precio  = 0 ;
	}
	
	/**
	 * @return the prodCarrito
	 */
	public List<Insumo> getProdCarrito() {
		return prodCarrito;
	}

	/**
	 * @param prodCarrito the prodCarrito to set
	 */
	public void setProdCarrito(List<Insumo> prodCarrito) {
		this.prodCarrito = prodCarrito;
	}

	/**
	 * @return the precio
	 */
	public int getPrecio()
	{
		int precio = 0;
		Iterator iterator = prodCarrito.iterator();
		while (iterator.hasNext())
		{
			Insumo insumo = (Insumo) iterator.next();
			precio += insumo.getPrecio();
		}
		return precio;
	}
	
	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(int precio) {
		this.precio = precio;
	}
}
