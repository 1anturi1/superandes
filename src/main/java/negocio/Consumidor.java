package negocio;

public class Consumidor implements VOConsumidor
{
	//---------------------------------------------------------------
	//CONSTANTES
	//---------------------------------------------------------------
	public final static String PERSONA_NATURAL = "NATURAL";
	public final static String EMPRESA = "EMPRESA";

	//---------------------------------------------------------------
	//ATRIBUTOS
	//---------------------------------------------------------------

	private int identificacion;

	private String direccion;

	private String correo ;

	private String nombre ;

	private int puntosAcumulados ;

	private String tipo;
	
	
	//---------------------------------------------------------------
	//METODOS
	//---------------------------------------------------------------

	/**
	 * @param correo
	 * @param nombre
	 * @param puntosAcumulados
	 * @param identificacion
	 * @param direccion
	 */
	public Consumidor(int identificacion, String correo, String nombre, int puntosAcumulados, String direccion, String tipo)
	{
		this.correo = correo;
		this.nombre = nombre;
		this.puntosAcumulados = puntosAcumulados;
		this.direccion = direccion;
		this.identificacion = identificacion;
		if(tipo.equalsIgnoreCase(EMPRESA))
			this.tipo = EMPRESA;
		else
			this.tipo = PERSONA_NATURAL;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		if(tipo.equalsIgnoreCase(EMPRESA))
			this.tipo = EMPRESA;
		else
			this.tipo = PERSONA_NATURAL;;
	}

	/**
	 * @return the identificacion
	 */
	public int getIdentificacion() {
		return identificacion;
	}

	/**
	 * @param identificacion the direccion to set
	 */
	public void setIdentificacion(int identificacion) {
		this.identificacion = identificacion;
	}

	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * @return the correo
	 */
	public String getCorreo() {
		return correo;
	}

	/**
	 * @param correo the correo to set
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the puntosAcumulados
	 */
	public int getPuntosAcumulados() {
		return puntosAcumulados;
	}

	/**
	 * @param puntosAcumulados the puntosAcumulados to set
	 */
	public void setPuntosAcumulados(int puntosAcumulados) {
		this.puntosAcumulados = puntosAcumulados;
	}


	@Override
	public String toString() {
		return "Consumidor [correo=" + correo + ", nombre=" + nombre + ", puntosAcumulados=" + puntosAcumulados + "]";
	}
}
