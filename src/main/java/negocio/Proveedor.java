package negocio;

import java.util.List;

public class Proveedor implements VOProveedor
{
	private int nit ;

	private String nombre ;

	private List listaProductos ;

	/**
	 * @param nit
	 * @param nombre
	 * @param listaProductos
	 */
	public Proveedor(int nit, String nombre, List listaProductos) {
		super();
		this.nit = nit;
		this.nombre = nombre;
		this.listaProductos = listaProductos;
	}

	/**
	 * @return the nit
	 */
	public int getNit() {
		return nit;
	}
	
	/**
	 * @param nit the nit to set
	 */
	public void setNit(int nit) {
		this.nit = nit;
	}
	
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * @return the listaProductos
	 */
	public List getListaProductos() {
		return listaProductos;
	}
	
	/**
	 * @param listaProductos the listaProductos to set
	 */
	public void setListaProductos(List listaProductos) {
		this.listaProductos = listaProductos;
	}
	

	
	@Override
	/**
	 * @return 
	 */
	public String toString()
	{
		return "" ;
	}
	
	
	
}
