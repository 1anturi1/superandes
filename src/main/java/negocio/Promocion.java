package negocio;

import java.util.Date;

public class Promocion  implements VOPromocion
{

	//---------------------------------------------------------------
	//CONSTANTES
	//---------------------------------------------------------------
	/**
	 * Pague n unidades y lleve m (n < m) (por ejemplo, pague 2, lleve 3).
	 * o Descuento de un determinado porcentaje. (por ejemplo, jabón Axión 200 ml tiene 20% de descuento)
	 * o Page x cantidad, lleve y (x < y) (por ejemplo, Café Buendía pague 170 gr lleve 200 gr)
	 * o Pague 1 y lleve el segundo por algún porcentaje del precio (50%, generalmente).
	 */

	public final static int PAGUE_N_LLEVE_M = 1;
	public final static int DESCUENTO_PORCENTAJE = 2;
	public final static int PAGUE_N_LLEVE_M_CANTIDAD = 3;
	public final static int LLEVE_SEGUNDO_PORCENTAJE = 4;

	//---------------------------------------------------------------
	//ATRIBUTOS
	//---------------------------------------------------------------

	public int id;

	public String nombre ;
	
	public String estado ;
	
	public Date fechaInicio ;
	
	public Date fechaFinal ;

	private String especificaciones ;
	
	private int tipoPromocion ;
	
	private Insumo insumo ;

	public Promocion()
	{
		id = 0;
		nombre = "" ;
		estado = "" ;
		fechaInicio = new Date() ;
		fechaFinal = new Date() ;
		especificaciones = "";
		tipoPromocion = 0 ;
		insumo = new Insumo() ;
	}
	
	/**
	 * @param id
	 * @param nombre
	 * @param estado
	 * @param fechaInicio
	 * @param fechaFinal
	 * @param especificaciones
	 * @param tipoPromocion
	 * @param insumo
	 */
	public Promocion(int id, String nombre, String estado, Date fechaInicio, Date fechaFinal, String especificaciones,
			int tipoPromocion, Insumo insumo) {

		this.id = id;
		this.nombre = nombre;
		this.estado = estado;
		this.fechaInicio = fechaInicio;
		this.fechaFinal = fechaFinal;
		this.especificaciones = especificaciones;
		this.tipoPromocion = tipoPromocion;
		this.insumo = insumo;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the fechaInicio
	 */
	public Date getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * @param fechaInicio the fechaInicio to set
	 */
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	/**
	 * @return the fechaFinal
	 */
	public Date getFechaFinal() {
		return fechaFinal;
	}

	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	/**
	 * @return the especificaciones
	 */
	public String getEspecificaciones() {
		return especificaciones;
	}

	/**
	 * @param especificaciones the especificaciones to set
	 */
	public void setEspecificaciones(String especificaciones) {
		this.especificaciones = especificaciones;
	}

	/**
	 * @return the tipoPromocion
	 */
	public int getTipoPromocion() {
		return tipoPromocion;
	}

	/**
	 * @param tipoPromocion the tipoPromocion to set
	 */
	public void setTipoPromocion(int tipoPromocion) {
		this.tipoPromocion = tipoPromocion;
	}

	/**
	 * @return the insumo
	 */
	public Insumo getInsumo() {
		return insumo;
	}

	/**
	 * @param insumo the insumo to set
	 */
	public void setInsumo(Insumo insumo) {
		this.insumo = insumo;
	}

	/** (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PromocionSucursal [nombre=" + nombre + ", estado=" + estado + ", fechaInicio=" + fechaInicio
				+ ", fechaFinal=" + fechaFinal + ", especificaciones=" + especificaciones + ", tipoPromocion="
				+ tipoPromocion + ", insumo=" + insumo + "]";
	}



	
	
}
