package negocio;

import java.util.ArrayList;
import java.util.List;

public class Sucursal 
{

	//---------------------------------------------------------------
	//ATRIBUTOS
	//---------------------------------------------------------------

	/*
	 * Nombre de la sucursal de un supermercado
	 */
	private String nombre ;

	/*
	 * Direccion de la sucursal
	 */
	private String direccion ;

	/*
	 * Ciudad en la que se encuentra la sucursal
	 */
	private String ciudad ;

	/*
	 * Lista de bodegas que hay en la sucursal 
	 */
	private List bodegas ;

	/*
	 * Lista de estantes que hay en la sucursal
	 */
	private List estantes ;

	//---------------------------------------------------------------
	//METODOS
	//---------------------------------------------------------------


	public Sucursal()
	{
		this.nombre = "" ;
		this.direccion = "" ;
		this.ciudad = "" ;
		this.bodegas = new ArrayList<>() ;
		this.estantes = new ArrayList<>() ;

	}

	public Sucursal(String pNombre, String pDireccion, String pCiudad)
	{
		this.nombre =pNombre;
		this.direccion = pDireccion;
		this.ciudad = pCiudad;
	}

/*
 * Metodo que retorna el nombre de la sucursal
 * @return el nombre
 */
	public String getNombre() 
	{
		return nombre;
	}

	/*
	 * Metodo que modifica el nombre de la sucursal 
	 * @param Nuevo nombre
	 */
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}

	
	/*
	 * Metodo que retorna la direccion de la sucursal
	 * @return direccion
	 */
	public String getDireccion() 
	{
		return direccion;
	}

	
	/*
	 * Metodo que modifica la direccion de la sucursal 
	 * @param nueva direccion
	 */
	public void setDireccion(String direccion) 
	{
		this.direccion = direccion;
	}

	/*
	 * Metodo que retorna la ciudad de la sucursal
	 * @return ciudad
	 */
	public String getCiudad() 
	{
		return ciudad;
	}

	/*
	 * Metodo que modifica la ciudad de la sucursal 
	 * @param nueva ciudad
	 */
	public void setCiudad(String ciudad) 
	{
		this.ciudad = ciudad;
	}

	/*
	 * Metodo que retorna las bodegas  de la sucursal
	 * @return bodegas
	 */
	public List getBodegas() 
	{
		return bodegas;
	}

	/*
	 * Metodo que modifica las bodegas de la sucursal 
	 * @param Lista de bodegas
	 */
	public void setBodegas(List bodegas) 
	{
		this.bodegas = bodegas;
	}
	/*
	 * Metodo que retorna los estantes  de la sucursal
	 * @return estantes
	 */
	public List getEstantes() 
	{
		return estantes;
	}

	/*
	 * Metodo que modifica los estantes de la sucursal 
	 * @param Lista de estantes
	 */
	public void setEstantes(List estantes) 
	{
		this.estantes = estantes;
	}


	@Override
	/**
	 * @return 
	 */
	public String toString()
	{
		return "Nombre: " + nombre ;
	}


}
