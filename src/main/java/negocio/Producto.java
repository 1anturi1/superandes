package negocio;

import java.util.ArrayList;
import java.util.List;

public class Producto implements VOProducto 
{
	//---------------------------------------------------------------
	//ATRIBUTOS
	//---------------------------------------------------------------


	private int codigoDeBarras ;
	private String nombre ;
	private int precio ;
	private String marca ;
	private double calificacionCalidad ;
	private String presentacion ;
	private int cantidadPresentacion ;
	private String clasificacion ;
	private int precioPorUnidadDeMedida ;
	private String tipoProducto ;
	private String unidadDeMedida ;
	private List proveedores ;


	//---------------------------------------------------------------
	//METODOS
	//---------------------------------------------------------------

	public Producto()
	{
		codigoDeBarras = 00;
		nombre = "";
		precio = 0;
		marca ="";
		calificacionCalidad  = 0.0;
		presentacion = "";
		cantidadPresentacion = 0;
		clasificacion ="";
		precioPorUnidadDeMedida =0;
		tipoProducto = "";
		unidadDeMedida = "";
		proveedores = new ArrayList<>();
	}


	/**
	 * @param codigoDeBarras
	 * @param nombre
	 * @param precio
	 * @param marca
	 * @param calificacionCalidad
	 * @param presentacion
	 * @param cantidadPresentacion
	 * @param clasificacion
	 * @param precioPorUnidadDeMedida
	 * @param tipoProducto
	 * @param unidadDeMedida
	 * @param proveedores
	 */
	public Producto(int codigoDeBarras, String nombre, int precio, String marca, double calificacionCalidad,
			String presentacion, int cantidadPresentacion, String clasificacion, int precioPorUnidadDeMedida,
			String tipoProducto, String unidadDeMedida, List proveedores) {

		this.codigoDeBarras = codigoDeBarras;
		this.nombre = nombre;
		this.precio = precio;
		this.marca = marca;
		this.calificacionCalidad = calificacionCalidad;
		this.presentacion = presentacion;
		this.cantidadPresentacion = cantidadPresentacion;
		this.clasificacion = clasificacion;
		this.precioPorUnidadDeMedida = precioPorUnidadDeMedida;
		this.tipoProducto = tipoProducto;
		this.unidadDeMedida = unidadDeMedida;
		this.proveedores = proveedores;
	}
	/**
	 * @return the codigoDeBarras
	 */
	public int getCodigoDeBarras() {
		return codigoDeBarras;
	}
	/**
	 * @param codigoDeBarras the codigoDeBarras to set
	 */
	public void setCodigoDeBarras(char codigoDeBarras) {
		this.codigoDeBarras = codigoDeBarras;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the precio
	 */
	public int getPrecio() {
		return precio;
	}
	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	/**
	 * @return the marca
	 */
	public String getMarca() {
		return marca;
	}
	/**
	 * @param marca the marca to set
	 */
	public void setMarca(String marca) {
		this.marca = marca;
	}
	/**
	 * @return the calificacionCalidad
	 */
	public double getCalificacionCalidad() {
		return calificacionCalidad;
	}
	/**
	 * @param calificacionCalidad the calificacionCalidad to set
	 */
	public void setCalificacionCalidad(double calificacionCalidad) {
		this.calificacionCalidad = calificacionCalidad;
	}
	/**
	 * @return the presentacion
	 */
	public String getPresentacion() {
		return presentacion;
	}
	/**
	 * @param presentacion the presentacion to set
	 */
	public void setPresentacion(String presentacion) {
		this.presentacion = presentacion;
	}
	/**
	 * @return the cantidadPresentacion
	 */
	public int getCantidadPresentacion() {
		return cantidadPresentacion;
	}
	/**
	 * @param cantidadPresentacion the cantidadPresentacion to set
	 */
	public void setCantidadPresentacion(int cantidadPresentacion) {
		this.cantidadPresentacion = cantidadPresentacion;
	}
	/**
	 * @return the clasificacion
	 */
	public String getClasificacion() {
		return clasificacion;
	}
	/**
	 * @param clasificacion the clasificacion to set
	 */
	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}
	/**
	 * @return the precioPorUnidadDeMedida
	 */
	public int getPrecioPorUnidadDeMedida() {
		return precioPorUnidadDeMedida;
	}
	/**
	 * @param precioPorUnidadDeMedida the precioPorUnidadDeMedida to set
	 */
	public void setPrecioPorUnidadDeMedida(int precioPorUnidadDeMedida) {
		this.precioPorUnidadDeMedida = precioPorUnidadDeMedida;
	}
	/**
	 * @return the tipoProducto
	 */
	public String getTipoProducto() {
		return tipoProducto;
	}
	/**
	 * @param tipoProducto the tipoProducto to set
	 */
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	/**
	 * @return the unidadDeMedida
	 */
	public String getUnidadDeMedida() {
		return unidadDeMedida;
	}
	/**
	 * @param unidadDeMedida the unidadDeMedida to set
	 */
	public void setUnidadDeMedida(String unidadDeMedida) {
		this.unidadDeMedida = unidadDeMedida;
	}
	/**
	 * @return the proveedores
	 */
	public List getProveedores() {
		return proveedores;
	}
	/**
	 * @param proveedores the proveedores to set
	 */
	public void setProveedores(List proveedores) {
		this.proveedores = proveedores;
	}


	@Override
	/**
	 * @return 
	 */
	public String toString()
	{
		return "" ;
	}




}
