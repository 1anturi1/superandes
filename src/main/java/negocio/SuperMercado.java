package negocio;

import org.apache.log4j.Logger;

import persistencia.PersistenciaSuperAndes;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.jdo.PersistenceManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Date;

public class SuperMercado implements VOSuperMercado
{
	
	//CLASE PRINCIPAL 

	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Logger para escribir la traza de la ejecución
	 */
	private static Logger log = Logger.getLogger(SuperMercado.class.getName());

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia
	 */
	private PersistenciaSuperAndes pp;

	/*
	 * Nombre del supermercado, ejemplo Exito, Carrefour, Carulla, IKEA, HomeCenter, etc.
	 */
	private String nombre ;
	/*
	 * Lista de sucursales del supermercado 
	 */
	private List sucursales ;

	//---------------------------------------------------------------
	//METODOS
	//---------------------------------------------------------------

	/*
	 * Constructor por default
	 */
	public SuperMercado()
	{
		this.nombre ="" ;
		this.sucursales = new ArrayList<Sucursal>() ;
	}
	/*
	 * Constructor con valores por parametro
	 */
	public SuperMercado(String pNombre, Sucursal pSucursal)
	{
		this.nombre =pNombre ;
		this.sucursales = new ArrayList<Sucursal>() ;
		this.sucursales.add(pSucursal) ;
	}

	/*
	 * (non-Javadoc)
	 * @see negocio.VOSuperMercado#getNombre()
	 */
	public String getNombre() {
		return nombre;
	}

	/*
	 * 
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/*
	 * (non-Javadoc)
	 * @see negocio.VOSuperMercado#getSucursales()
	 */
	public List getSucursales() {
		return sucursales;
	}

	/*
	 * 
	 */
	public void setSucursales(List sucursales) {
		this.sucursales = sucursales;
	}

	/* ****************************************************************
	 * 			Métodos para manejar los PROVEEDORES
	 *****************************************************************/

	/**
	 * Adiciona de manera persistente un proveedor
	 * Adiciona entradas al log de la aplicación
	 * @param nombre - El nombre del proveedor
	 * @return El objeto Proveedor adicionado. null si ocurre alguna Excepción
	 */
	public Proveedor registrarProveedor (String nombre)
	{
		log.info ("Adicionando Proveedor: " + nombre);
		Proveedor proveedor = pp.registrarProveedor (nombre);
		log.info ("Adición de tipo proveedor: " + proveedor);
		return proveedor;
	}

	/* ****************************************************************
	 * 			Métodos para manejar los PRODUCTOS
	 *****************************************************************/

	/**
	 * Adiciona de manera persistente un proveedor
	 * Adiciona entradas al log de la aplicación
	 * @param nombre - El nombre del producto
	 * @param precio - El precio del producto
	 * @param marca - La marca del producto
	 * @param calificacionCalidad - La clasificación de calodad del producto
	 * @param presentacion - La presentación del producto
	 * @param cantidadPresentacion - La cantidad por presentación del producto
	 * @param clasificacion - La clasificación del producto
	 * @param precioPorUnidadDeMedida - El precio por unidad de medida del producto
	 * @param tipoProducto - El tipo del producto
	 * @param unidadDeMedida - La unidad de medida del producto
	 * @return El objeto Producto adicionado. null si ocurre alguna Excepción
	 */
	public Producto adicionarProducto (String nombre, int precio, String marca,
										double calificacionCalidad, String presentacion,
										int cantidadPresentacion, String clasificacion,
										int precioPorUnidadDeMedida, String tipoProducto,
										String unidadDeMedida)
	{
		log.info ("Adicionando Producto: " + nombre);
		Producto producto = pp.adicionarProducto(nombre, precio, marca, calificacionCalidad, presentacion,
													cantidadPresentacion, clasificacion, precioPorUnidadDeMedida,
													tipoProducto, unidadDeMedida);
		log.info ("Adició de tipo producto: " + producto);
		return producto;
	}

	/* ****************************************************************
	 * 			Métodos para manejar los CONSUMIDOR
	 *****************************************************************/

	/**
	 * Adiciona de manera persistente un consumidor
	 * Adiciona entradas al log de la aplicación
	 * @param nombre - El nombre del consumidor
	 * @param direccion - La dirección del consumidor
	 * @param correo - El correo del consumidor
	 * @param puntosAcumulados - Los puntos acumulados del consumidor
	 * @param tipo - El tipo del consumidor
	 * @return El objeto Producto adicionado. null si ocurre alguna Excepción
	 */
	public Consumidor registrarConsumidor (String nombre, String direccion, String correo, int puntosAcumulados, String tipo)
	{
		log.info ("Adicionando Consumidor: " + nombre);
		Consumidor consumidor = pp.registrarConsumidor(nombre, direccion, correo, puntosAcumulados, tipo);
		log.info ("Adició de tipo consumidor: " + consumidor);
		return consumidor;
	}

	/* ****************************************************************
	 * 			Métodos para manejar las SUCURSALES
	 *****************************************************************/

	/**
	 * Adiciona de manera persistente una sucursal
	 * Adiciona entradas al log de la aplicación
	 * @param nombre - El nombre de la sucursal
	 * @param ciudad - La ciudad de la sucursal
	 * @param direccion - La dirección de la sucursal
	 * @return El objeto Producto adicionado. null si ocurre alguna Excepción
	 */
	public Sucursal registrarSucursal (String nombre, String ciudad, String direccion)
	{
		log.info ("Adicionando sucursal: " + nombre);
		Sucursal sucursal = pp.registrarSucursal(nombre, ciudad, direccion);
		log.info ("Adició de tipo sucursal: " + sucursal);
		return sucursal;
	}

	/* ****************************************************************
	 * 			Métodos para manejar las BODEGAS
	 *****************************************************************/

	/**
	 * Adiciona de manera persistente una bodega
	 * Adiciona entradas al log de la aplicación
	 * @param idSucursal - El identificador de la sucursal a la cual pertenece la bodega
	 * @param cantidadActual - La cantidad de productos que hay en la blodega
	 * @param capacidadPeso - La capasidad en peso de la bodega
	 * @param capacidadVolumen - La capasidad en volumen de la bodega
	 * @param tipoProducto - El tipo de producto de la bodega
	 * @return El objeto Producto adicionado. null si ocurre alguna Excepción
	 */
	public Bodega registrarBodega(int idSucursal, int cantidadActual, int capacidadPeso, int capacidadVolumen, String tipoProducto)
	{
		log.info ("Adicionando bodega a la sucursal: " + idSucursal);
		Bodega bodega = pp.registrarBodega(idSucursal, cantidadActual, capacidadPeso, capacidadVolumen, tipoProducto);
		log.info ("Adició de tipo bodega: " + bodega);
		return bodega;
	}

	/* ****************************************************************
	 * 			Métodos para manejar las BODEGAS
	 *****************************************************************/
	/**
	 * Adiciona de manera persistente una bodega
	 * Adiciona entradas al log de la aplicación
	 * @param idSucursal - El identificador de la sucursal a la cual pertenece el estante
	 * @param cantidadActual - La cantidad de productos que hay en la blodega
	 * @param capacidadPeso - La capasidad en peso de la bodega
	 * @param capacidadVolumen - La capasidad en volumen de la bodega
	 * @param nivelAbastecimiento -  El nivel de abastecimiento del estante
	 * @param tipoProducto - El tipo de producto de la bodega
	 * @return El objeto Producto adicionado. null si ocurre alguna Excepción
	 */
	public Estante registrarEstante(int idSucursal, int cantidadActual, int capacidadPeso, int capacidadVolumen, int nivelAbastecimiento,String tipoProducto)
	{
		log.info ("Adicionando estante a la sucursal: " + idSucursal);
		Estante estante = pp.registrarEstante(idSucursal, cantidadActual, capacidadPeso, capacidadVolumen, nivelAbastecimiento, tipoProducto);
		log.info ("Adició de tipo bodega: " + estante);
		return estante;
	}

    /* ****************************************************************
     * 			Métodos para manejar las PROMOCIONES
     *****************************************************************/

    /**
     * Adiciona de manera persistente una promoción
     * Adiciona entradas al log de la aplicación
     * @param idSucursal - El identificador de la sucursal a la cual pertenece la promoción
     * @param nombre - El nombre de la promoción
     * @param estado - El estado de la promoción
     * @param fechaInicio - La fecha de inicio de la promoción
     * @param fechaFinal - La fecha de fin de la promoción
     * @param especificaciones - Las especificiaciones de la promoción
     * @param tipoPromocion - El tipo de la promoción
     * @param cantidadVendidos - La cantidad de productos vendidos
     * @param insumo - El insumo de la promoción
     * @return El objeto Producto adicionado. null si ocurre alguna Excepción
     */
    public Promocion registrarPromocion(int idSucursal, String nombre, String estado,
										java.sql.Date fechaInicio, java.sql.Date fechaFinal, String especificaciones, int tipoPromocion,
										int insumo, int cantidadVendidos)
    {
        log.info ("Adicionando la promoción: " +  nombre + " a la sucursal: " + idSucursal);
        Promocion promocion = pp.registrarPromocion(idSucursal, nombre, estado, fechaInicio, fechaFinal, especificaciones,
                                                    tipoPromocion, insumo, cantidadVendidos);
        log.info ("Adició de tipo promoción: " + promocion);
        return promocion;
    }

    /**
     * Elimina una promoción por su identificador
     * Adiciona entradas al log de la aplicación
     *  @param idPromocion - El identificador de la promoción
     * @return El número de tuplas eliminadas (1 o 0)
     */
    public int eliminarPromocionPorId (int idPromocion)
    {
        log.info ("Eliminando promoción por id: " + idPromocion);
        int resp = pp.eliminarPromocionPorId(idPromocion);
        log.info ("Eliminando promoción por id: " + resp + " tuplas eliminadas");
        return resp;
    }

    /**
     * Elimina las promociones vencidas o que no tienen productos por su identificador
     * Adiciona entradas al log de la aplicación
     * @return El número de tuplas eliminadas (1 o 0)
     */
    public int eliminarPromVencidaAcabada()
    {
        ArrayList<Promocion> promociones = (ArrayList<Promocion>) pp.darPromociones();
		int tuplasEliminadas = 0;
        Iterator iterator = promociones.iterator();
        while(iterator.hasNext())
        {
        	Promocion prom = (Promocion) iterator.next();
			Date ahora = new Date( );
			if(prom.getFechaFinal().compareTo(ahora) == 1)
			{
				tuplasEliminadas++;
				eliminarPromocionPorId(prom.getId());
			}

			if(pp.darInsumoPorId(prom.getInsumo().getCodigoDeBarras()) == null)
			{
				tuplasEliminadas++;
				eliminarPromocionPorId(prom.getId());
			}
        }
        return tuplasEliminadas;
    }

    
    public List<VOProveedor> darVOProveedor ()
	{     
        List<VOProveedor> voProv = new LinkedList<VOProveedor> ();
        for (Proveedor tb : pp.darProveedores())
        {
        	voProv.add (tb);
        }
        return voProv;
	}

    
    
    
  
	public boolean adicionarProductoACarrito(int idCarrito, int codigoBarrasInsumo)
	{
		log.info ("Adicionando producto a carrito: " + idCarrito);
		boolean adic = pp.adicionarProductoACarrito(idCarrito, codigoBarrasInsumo) ;
		
		log.info ("Adiciono el producto: " + adic);
		return adic;
	}
    
    
    public boolean eliminarProductoACarrito(PersistenceManager pm,  int idCarrito, int codigoBarrasInsumo)
    {
    	log.info ("Eliminando producto a carrito: " + idCarrito);
        boolean resp = pp.eliminarProductoACarrito(idCarrito, codigoBarrasInsumo); 
        
		log.info ("Adiciono el producto: " + resp);
        return resp;
    }
    
    
    
   
	/* ****************************************************************
	 * 			Métodos para manejar los INSUMOS
	 *****************************************************************/

	/**
	 * Registra la llegada de un pedido a una sucursal
	 * Adiciona entradas al log de la aplicación
	 * @param idSucursal - El identificador de la sucursal a la cual pertenece la promoción
	 * @param idInsumo - El identificador del insumo
	 * @return True si se registra la llegada. false si ocurre alguna Excepción
	 */
	public boolean registrarLlegadaPedido(int idSucursal, int idInsumo)
	{
		Insumo insumo = pp.darInsumoPorId(idInsumo);
		int idBodega = pp.darBodegaPorTipo(insumo.getTipoProducto());
		log.info ("Registrando llegada de insumo: " +  idInsumo + " a la sucursal: " + idSucursal);
		boolean registro = pp.registrarBodegaInsumo(idBodega, idInsumo);
		log.info ("Registrada llegada de insumo: " +  idInsumo + " a la sucursal: " + idSucursal);
		return registro;
	}

	/* ****************************************************************
	 * 			Métodos para manejar las VENTAS
	 *****************************************************************/

	/**
	 * Adiciona de manera persistente una venta
	 * Adiciona entradas al log de la aplicación
	 * @param idSucursal - El identificador de la sucursal a la cual pertenece la venta
	 * @param costoVenta - El costo total de la venta
	 * @param estado - El estado de la venta
	 * @param puntos - Los puntos de la venta
	 * @param idConsumidor - El id del consumidor
	 * @param fecha - fecha de la venta
	 * @return El objeto Venta adicionado. null si ocurre alguna Excepción
	 */
	public Venta registrarVenta(int idSucursal, int idConsumidor, int costoVenta,
									String estado, int puntos, java.util.Date fecha)
	{
		log.info ("Adicionando una venta a la sucursal: " + idSucursal);
		Venta venta = pp.registrarVenta(idSucursal, idConsumidor, costoVenta, estado, puntos, fecha);
		log.info ("Adició de tipo promoción: " + venta);
		return venta;
	}

	/**
	 * Elimina un insumo por su identificador
	 * Adiciona entradas al log de la aplicación
	 *  @param idInsumo - El identificador de la promoción
	 * @return El número de tuplas eliminadas (1 o 0)
	 */
	public int eliminarInsumoPorId (int idInsumo)
	{
		log.info ("Eliminando promoción por id: " + idInsumo);
		int resp = pp.eliminarInsumoPorId(idInsumo);
		log.info ("Eliminando promoción por id: " + resp + " tuplas eliminadas");
		return resp;
	}

	@Override
	/**
	 * @return Una cadena de caracteres con el nombre del SuperMercado y la cantidad de sucursales
	 */
	public String toString()
	{
		return "Nombre: " + nombre + "\n"+ "Cantidad de sucursales: " + sucursales.size() ;
	}

	public static String getFechaActual() {
		Date ahora = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
		return formateador.format(ahora);
	}

}
