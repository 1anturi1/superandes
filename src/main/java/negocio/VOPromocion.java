package negocio;

import java.util.Date;

public interface VOPromocion {

	/**
	 * @return the nombre
	 */
	public String getNombre();

	/**
	 * @return the id
	 */
	public int getId();

	/**
	 * @return the estado
	 */
	public String getEstado();

	/**
	 * @return the fechaInicio
	 */
	public Date getFechaInicio();

	/**
	 * @return the fechaFinal
	 */
	public Date getFechaFinal();

	/**
	 * @return the especificaciones
	 */
	public String getEspecificaciones();

	/**
	 * @return the tipoPromocion
	 */
	public int getTipoPromocion();

	/**
	 * @return the insumo
	 */
	public Insumo getInsumo();
	
	@Override
	public String toString() ;
	
}
