package negocio;

import java.util.List;

public interface VOEstante 
{
	/**
	 * @return the cantidadActual
	 */
	public int getCantidadActual();
	

	/**
	 * @return the capacidadPeso
	 */
	public int getCapacidadPeso();
	

	/**
	 * @return the capacidadVolumen
	 */
	public int getCapacidadVolumen() ;
	

	/**
	 * @return the nivelAbastecimiento
	 */
	public int getNivelAbastecimiento() ;
	
	
	/**
	 * @return the tipoProducto
	 */
	public String getTipoProducto();
	
	

	/**
	 * @return the insumosEst
	 */
	public List<Insumo> getInsumosEst() ;


	/**
	 * @param insumosEst the insumosEst to set
	 */
	public void setInsumosEst(List<Insumo> insumosEst) ;
	
	
	@Override
	/**
	 * @return 
	 */
	public String toString();
	
	
}
