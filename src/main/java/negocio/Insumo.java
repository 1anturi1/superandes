package negocio;

import java.util.List;

public class Insumo extends Producto implements VOInsumo
{
	//---------------------------------------------------------------
	//ATRIBUTOS
	//---------------------------------------------------------------

	private  int nivelReordenSucursal ;

	private int pesoEmpaque ;

	private int volumenEmpaque ;

	//---------------------------------------------------------------
	//METODOS
	//---------------------------------------------------------------

	/**
	 * 
	 */
	public Insumo() 
	{
		super();
		nivelReordenSucursal = 0 ;
		pesoEmpaque = 0 ;
		volumenEmpaque = 0 ;
	}

	/**
	 * @param codigoDeBarras
	 * @param nombre
	 * @param precio
	 * @param marca
	 * @param calificacionCalidad
	 * @param presentacion
	 * @param cantidadPresentacion
	 * @param clasificacion
	 * @param precioPorUnidadDeMedida
	 * @param tipoProducto
	 * @param unidadDeMedida
	 * @param proveedores
	 * @param nivelReordenSucursal
	 * @param pesoEmpaque
	 * @param volumenEmpaque
	 */
	public Insumo(char codigoDeBarras, String nombre, int precio, String marca, double calificacionCalidad,
			String presentacion, int cantidadPresentacion, String clasificacion, int precioPorUnidadDeMedida,
			String tipoProducto, String unidadDeMedida, List proveedores,int nivelReordenSucursal, int pesoEmpaque, int volumenEmpaque) 
	{
		super(codigoDeBarras, nombre, precio, marca, calificacionCalidad, presentacion, cantidadPresentacion, clasificacion,
				precioPorUnidadDeMedida, tipoProducto, unidadDeMedida, proveedores);
		this.nivelReordenSucursal = nivelReordenSucursal;
		this.pesoEmpaque = pesoEmpaque;
		this.volumenEmpaque = volumenEmpaque;

	}

	/**
	 * @return the nivelReordenSucursal
	 */
	public int getNivelReordenSucursal() {
		return nivelReordenSucursal;
	}

	/**
	 * @param nivelReordenSucursal the nivelReordenSucursal to set
	 */
	public void setNivelReordenSucursal(int nivelReordenSucursal) {
		this.nivelReordenSucursal = nivelReordenSucursal;
	}

	/**
	 * @return the pesoEmpaque
	 */
	public int getPesoEmpaque() {
		return pesoEmpaque;
	}

	/**
	 * @param pesoEmpaque the pesoEmpaque to set
	 */
	public void setPesoEmpaque(int pesoEmpaque) {
		this.pesoEmpaque = pesoEmpaque;
	}

	/**
	 * @return the volumenEmpaque
	 */
	public int getVolumenEmpaque() {
		return volumenEmpaque;
	}

	/**
	 * @param volumenEmpaque the volumenEmpaque to set
	 */
	public void setVolumenEmpaque(int volumenEmpaque) {
		this.volumenEmpaque = volumenEmpaque;
	}



	@Override
	/**
	 * @return 
	 */
	public String toString()
	{
		return "" ;
	}


}
