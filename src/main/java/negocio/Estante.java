package negocio;

import java.util.ArrayList;
import java.util.List;

public class Estante implements VOEstante
{
	//---------------------------------------------------------------
	//ATRIBUTOS
	//---------------------------------------------------------------

	/**
	 * 
	 */
	private int cantidadActual;
	
	private int capacidadPeso ;
	
	private int capacidadVolumen ;
	
	private int nivelAbastecimiento ;
	
	private String tipoProducto ;
	
	private List<Insumo> insumosEst;
	
	//---------------------------------------------------------------
	//METODOS
	//---------------------------------------------------------------

	/**
	 * Constructor por default
	 */
	public Estante()
	{
		cantidadActual =0 ;
		capacidadPeso = 0 ;
		capacidadVolumen = 0 ;
		nivelAbastecimiento = 0 ;
		tipoProducto = "" ;
		insumosEst = new ArrayList<>(); 
		
	}


	/**
	 * @param pCantidadActual
	 * @param pCapacidadPeso
	 * @param pCapacidadVolumen
	 * @param pNivelAbastecimiento
	 * @param pTipoProducto
	 */
	public Estante(int pCantidadActual, int pCapacidadPeso, int pCapacidadVolumen,
			int pNivelAbastecimiento, String pTipoProducto)
	{
		this.cantidadActual = pCantidadActual;
		this.capacidadPeso = pCapacidadPeso;
		this.capacidadVolumen = pCapacidadVolumen;
		this.nivelAbastecimiento = pNivelAbastecimiento;
		this.tipoProducto = pTipoProducto;
	}

	/**
	 * @return the cantidadActual
	 */
	public int getCantidadActual() {
		return cantidadActual;
	}


	/**
	 * @param cantidadActual the cantidadActual to set
	 */
	public void setCantidadActual(int cantidadActual) {
		this.cantidadActual = cantidadActual;
	}


	/**
	 * @return the capacidadPeso
	 */
	public int getCapacidadPeso() {
		return capacidadPeso;
	}


	/**
	 * @param capacidadPeso the capacidadPeso to set
	 */
	public void setCapacidadPeso(int capacidadPeso) {
		this.capacidadPeso = capacidadPeso;
	}


	/**
	 * @return the capacidadVolumen
	 */
	public int getCapacidadVolumen() {
		return capacidadVolumen;
	}


	/**
	 * @param capacidadVolumen the capacidadVolumen to set
	 */
	public void setCapacidadVolumen(int capacidadVolumen) {
		this.capacidadVolumen = capacidadVolumen;
	}


	/**
	 * @return the nivelAbastecimiento
	 */
	public int getNivelAbastecimiento() {
		return nivelAbastecimiento;
	}


	/**
	 * @param nivelAbastecimiento the nivelAbastecimiento to set
	 */
	public void setNivelAbastecimiento(int nivelAbastecimiento) {
		this.nivelAbastecimiento = nivelAbastecimiento;
	}


	/**
	 * @return the tipoProducto
	 */
	public String getTipoProducto() {
		return tipoProducto;
	}


	/**
	 * @param tipoProducto the tipoProducto to set
	 */
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	
	
	
	
	/**
	 * @return the insumosEst
	 */
	public List<Insumo> getInsumosEst() {
		return insumosEst;
	}


	/**
	 * @param insumosEst the insumosEst to set
	 */
	public void setInsumosEst(List<Insumo> insumosEst) {
		this.insumosEst = insumosEst;
	}


	@Override
	/**
	 * @return 
	 */
	public String toString()
	{
		return "" ;
	}
	

	
}
