package negocio;

import java.util.List;

public interface VOBodega 
{

	/**
	 * @return the cantidadActual
	 */
	public int getCantidadActual()  ;
	
	/**
	 * @return the capacidadPeso
	 */
	public int getCapacidadPeso();
	
	/**
	 * @return the capacidadVolumen
	 */
	public int getCapacidadVolumen();
	
	/**
	 * @return the tipoProducto
	 */
	public String getTipoProducto();
	

	/**
	 * @return the insumosBod
	 */
	public List<Insumo> getInsumosBod() ;
	
	
	/**
	 * @param insumosBod the insumosBod to set
	 */
	public void setInsumosBod(List<Insumo> insumosBod) ;
	
	@Override
	/**
	 * @return 
	 */
	public String toString() ;
	
}
