package negocio;

import java.util.List;

public interface VOProducto {
	/**
	 * @return the codigoDeBarras
	 */
	public int getCodigoDeBarras();
	
	/**
	 * @return the nombre
	 */
	public String getNombre();
	
	/**
	 * @return the precio
	 */
	public int getPrecio() ;
	
	/**
	 * @return the marca
	 */
	public String getMarca();
	
	/**
	 * @return the calificacionCalidad
	 */
	public double getCalificacionCalidad() ;
	
	/**
	 * @return the presentacion
	 */
	public String getPresentacion() ;
	
	/**
	 * @return the cantidadPresentacion
	 */
	public int getCantidadPresentacion();
	
	/**
	 * @return the clasificacion
	 */
	public String getClasificacion();
	
	/**
	 * @return the precioPorUnidadDeMedida
	 */
	public int getPrecioPorUnidadDeMedida();
	
	/**
	 * @return the tipoProducto
	 */
	public String getTipoProducto();
	
	/**
	 * @return the unidadDeMedida
	 */
	public String getUnidadDeMedida();
	
	/**
	 * @return the proveedores
	 */
	public List getProveedores() ;
	

	@Override
	/**
	 * @return 
	 */
	public String toString() ;

}
