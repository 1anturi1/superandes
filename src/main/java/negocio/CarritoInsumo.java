package negocio;

public class CarritoInsumo 
{

	private int idCarrito ;
	
	private int codInsumo ;

	
	
	
	/**
	 * @param idCarrito
	 * @param idInsumo
	 */
	public CarritoInsumo(int idCarrito, int idInsumo) 
	{
		this.idCarrito = idCarrito;
		this.codInsumo = idInsumo;
	}

	/**
	 * @return the idCarrito
	 */
	public int getIdCarrito() {
		return idCarrito;
	}

	/**
	 * @param idCarrito the idCarrito to set
	 */
	public void setIdCarrito(int idCarrito) {
		this.idCarrito = idCarrito;
	}

	/**
	 * @return the idInsumo
	 */
	public int getIdInsumo() {
		return codInsumo;
	}

	/**
	 * @param idInsumo the idInsumo to set
	 */
	public void setIdInsumo(int idInsumo) {
		this.codInsumo = idInsumo;
	}
	
	
	
	
	
}
