package negocio;

import java.util.Date;
import java.util.List;

public class Venta implements VOVenta{

	//---------------------------------------------------------------
	//ATRIBUTOS
	//---------------------------------------------------------------

	private int id;

	private int costoTotal ;

	private String estado ;

	private Date fecha ;

	private int puntosVenta ;

	//---------------------------------------------------------------
	//METODOS
	//---------------------------------------------------------------

	/**
	 * Constructor por default
	 */
	public Venta()
	{
		id = 0;
		costoTotal = 0 ;
		estado = "" ;
		fecha = new Date ();
		puntosVenta = 0 ;
	}


	/**
	 * @param costoTotal
	 * @param estado
	 * @param fecha
	 * @param puntosVenta
	 */
	public Venta(int id, int costoTotal, String estado, Date fecha, int puntosVenta) {

		this.id = id;
		this.costoTotal = costoTotal;
		this.estado = estado;
		this.fecha = fecha;
		this.puntosVenta = puntosVenta;
	}

	/**
	 * @return the costoTotal
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the costoTotal to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the costoTotal
	 */
	public int getCostoTotal() {
		return costoTotal;
	}

	/**
	 * @param costoTotal the costoTotal to set
	 */
	public void setCostoTotal(int costoTotal) {
		this.costoTotal = costoTotal;
	}

	/**
	 * @return the estado
	 */
	public String isEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the productos
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the productos to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the puntosVenta
	 */
	public int getPuntosVenta() {
		return puntosVenta;
	}

	/**
	 * @param puntosVenta the puntosVenta to set
	 */
	public void setPuntosVenta(int puntosVenta) {
		this.puntosVenta = puntosVenta;
	}

	@Override
	public String toString() {
		return "Venta [costoTotal=" + costoTotal + ", estado=" + estado + ", productos=" + fecha + ", puntosVenta="
				+ puntosVenta + "]";
	}

}
