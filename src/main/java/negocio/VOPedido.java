package negocio;

public interface VOPedido {
	/**
	 * @return the cantidadProductos
	 */
	public int getCantidadProductos() ;
	
	/**
	 * @return the costo
	 */
	public int getCosto() ;
	
	/**
	 * @return the estado
	 */
	public String getEstado();
	
	/**
	 * @return the producto
	 */
	public int getProducto()  ;
	
	
	@Override
	/**
	 * @return 
	 */
	public String toString() ;
	
}
