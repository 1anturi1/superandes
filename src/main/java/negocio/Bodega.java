package negocio;

import java.util.ArrayList;
import java.util.List;

//Prueba commit
//Prueba Commit IntelliJ
public class Bodega implements VOBodega
{
	//---------------------------------------------------------------
	//ATRIBUTOS
	//---------------------------------------------------------------


	private int cantidadActual;

	private int capacidadPeso ;

	private int capacidadVolumen ;

	private String tipoProducto ;
	
	private List<Insumo> insumosBod ;

	//---------------------------------------------------------------
	//METODOS
	//---------------------------------------------------------------


	/**
	 * @param cantidadActual
	 * @param capacidadPeso
	 * @param capacidadVolumen
	 * @param tipoProducto
	 */
	public Bodega(int cantidadActual, int capacidadPeso, int capacidadVolumen, String tipoProducto) 
	{
		this.cantidadActual = cantidadActual;
		this.capacidadPeso = capacidadPeso;
		this.capacidadVolumen = capacidadVolumen;
		this.tipoProducto = tipoProducto;
		this.insumosBod = new ArrayList<>() ;
	}

	/**
	 * @return the cantidadActual
	 */
	public int getCantidadActual() {
		return cantidadActual;
	}

	/**
	 * @param cantidadActual the cantidadActual to set
	 */
	public void setCantidadActual(int cantidadActual) {
		this.cantidadActual = cantidadActual;
	}

	/**
	 * @return the capacidadPeso
	 */
	public int getCapacidadPeso() {
		return capacidadPeso;
	}

	/**
	 * @param capacidadPeso the capacidadPeso to set
	 */
	public void setCapacidadPeso(int capacidadPeso) {
		this.capacidadPeso = capacidadPeso;
	}

	/**
	 * @return the capacidadVolumen
	 */
	public int getCapacidadVolumen() {
		return capacidadVolumen;
	}

	/**
	 * @param capacidadVolumen the capacidadVolumen to set
	 */
	public void setCapacidadVolumen(int capacidadVolumen) {
		this.capacidadVolumen = capacidadVolumen;
	}

	/**
	 * @return the tipoProducto
	 */
	public String getTipoProducto() {
		return tipoProducto;
	}

	/**
	 * @param tipoProducto the tipoProducto to set
	 */
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}


	
	
	/**
	 * @return the insumosBod
	 */
	public List<Insumo> getInsumosBod() {
		return insumosBod;
	}

	/**
	 * @param insumosBod the insumosBod to set
	 */
	public void setInsumosBod(List<Insumo> insumosBod) {
		this.insumosBod = insumosBod;
	}

	@Override
	/**
	 * @return 
	 */
	public String toString()
	{
		return "" ;
	}
	


}
