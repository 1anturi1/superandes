package negocio;

import java.util.List;

public interface VOSuperMercado 
{
	/*
	 * Retorna el nombre del supermercado
	 */
	public String getNombre() ;
	/*
	 * Retorna la lista de sucursales asociadas al supermercado
	 */
	public List getSucursales() ;

	@Override
	/**
	 * @return Una cadena de caracteres con el nombre del SuperMercado y la cantidad de sucursales
	 */

	public String toString();

}
