package negocio;

public interface VOPersonaNatural 
{
	/**
	 * @return the cedula
	 */
	public int getId()  ;
	
	/**
	 * @return the correo
	 */
	public String getCorreo() ;

	/**
	 * @return the nombre
	 */
	public String getNombre() ;
	
	
	/**
	 * @return the puntosAcumulados
	 */
	public int getPuntosAcumulados() ;
	
	
	@Override
	public String toString() ;
}
