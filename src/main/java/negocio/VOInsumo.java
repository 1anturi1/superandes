package negocio;

public interface VOInsumo 
{

	/**
	 * @return the nivelReordenSucursal
	 */
	public int getNivelReordenSucursal() ;

	/**
	 * @return the pesoEmpaque
	 */
	public int getPesoEmpaque() ;

	
	/**
	 * @return the volumenEmpaque
	 */
	public int getVolumenEmpaque() ;


	@Override
	/**
	 * @return 
	 */
	public String toString() ;
}
