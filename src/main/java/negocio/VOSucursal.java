package negocio;

import java.util.List;

public interface VOSucursal 
{

	public String getNombre()  ;
	
	public String getDireccion() ;
	
	public String getCiudad()  ;
	
	public List getBodegas()  ;
	
	public List getEstantes()  ;
	
	@Override
	/**
	 * @return 
	 */
	public String toString() ;
}
