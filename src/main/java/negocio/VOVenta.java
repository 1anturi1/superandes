package negocio;

import java.util.Date;

public interface VOVenta {

	/**
	 * @return the id
	 */
	public int getId();

	/**
	 * @return the costoTotal
	 */
	public int getCostoTotal();
	
	/**
	 * @return the estado
	 */
	public String isEstado();
	
	/**
	 * @return the fecha
	 */
	public Date getFecha() ;
	
	/**
	 * @return the puntosVenta
	 */
	public int getPuntosVenta() ;
	
	@Override
	public String toString() ;

}
