package negocio;

import java.util.List;

public interface VOProveedor 
{

	/**
	 * @return the nit
	 */
	public int getNit() ;
	
	/**
	 * @return the nombre
	 */
	public String getNombre();
	

	/**
	 * @return the listaProductos
	 */
	public List getListaProductos() ;
	

	@Override
	/**
	 * @return 
	 */
	public String toString() ; 
}
