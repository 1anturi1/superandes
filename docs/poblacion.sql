

insert into SUCURSAL (id, NOMBRE, CIUDAD, DIRECCION) values (1, 'Jayo', 'Banjar Triwangsakeliki', '60869 Stoughton Crossing');
insert into SUCURSAL (id, NOMBRE, CIUDAD, DIRECCION) values (2, 'InnoZ', 'Bagong Sikat', '06754 Holmberg Parkway');
insert into SUCURSAL (id, NOMBRE, CIUDAD, DIRECCION) values (3, 'Photojam', 'Anuradhapura', '3 Maple Circle');
insert into SUCURSAL (id, NOMBRE, CIUDAD, DIRECCION) values (4, 'Zazio', 'Milaor', '18268 Sommers Park');
insert into SUCURSAL (id, NOMBRE, CIUDAD, DIRECCION) values (5, 'Trunyx', 'Sv. Ana v Slov. Goricah', '569 Schlimgen Lane');
insert into SUCURSAL (id, NOMBRE, CIUDAD, DIRECCION) values (6, 'Jatri', 'Gandzak', '95301 Almo Junction');
insert into SUCURSAL (id, NOMBRE, CIUDAD, DIRECCION) values (7, 'Midel', 'N.dalatando', '07810 Orin Lane');
insert into SUCURSAL (id, NOMBRE, CIUDAD, DIRECCION) values (8, 'Photojam', 'Szynwald', '20292 Cambridge Point');
insert into SUCURSAL (id, NOMBRE, CIUDAD, DIRECCION) values (9, 'Yozio', 'Yerazgavors', '955 Heffernan Drive');
insert into SUCURSAL (id, NOMBRE, CIUDAD, DIRECCION) values (10, 'Yabox', 'Barrancas', '664 Orin Parkway');



insert into BODEGA (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANT_ACTUAL,ID_SUCURSAL) values (1, 5, 45, 'ASEO', 1, 5);
insert into BODEGA (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANT_ACTUAL,ID_SUCURSAL) values (2, 22, 13, 'NO_PERECEDERO', 26, 6);
insert into BODEGA (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANT_ACTUAL,ID_SUCURSAL) values (3, 7, 95, 'HERRAMIENTAS', 36, 7);
insert into BODEGA (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANT_ACTUAL,ID_SUCURSAL) values (4, 81, 96, 'NO_PERECEDERO', 75, 8);
insert into BODEGA (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANT_ACTUAL,ID_SUCURSAL) values (5, 65, 26, 'ELECTRODOMESTICOS', 74, 9);
insert into BODEGA (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANT_ACTUAL,ID_SUCURSAL) values (6, 97, 11, 'ELECTRODOMESTICOS', 5, 1);
insert into BODEGA (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANT_ACTUAL,ID_SUCURSAL) values (7, 53, 82, 'ABARROTES', 39, 2);
insert into BODEGA (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANT_ACTUAL,ID_SUCURSAL) values (8, 40, 28, 'HERRAMIENTAS', 95, 3);
insert into BODEGA (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANT_ACTUAL,ID_SUCURSAL) values (9, 73, 34, 'MUEBLES', 86, 4);
insert into BODEGA (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANT_ACTUAL,ID_SUCURSAL) values (10, 34, 63, 'ABARROTES', 16, 10);







insert into ESTANTE (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANTIDAD_PRODUCTOS, CANT_ACTUAL, NIVEL_ABASTECIMIENTO) values (1, 75, 53, 'NO_PERECEDERO', 32, 26, 45);
insert into ESTANTE (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANTIDAD_PRODUCTOS, CANT_ACTUAL, NIVEL_ABASTECIMIENTO) values (2, 92, 87, 'ELECTRODOMESTICOS', 12, 29, 49);
insert into ESTANTE (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANTIDAD_PRODUCTOS, CANT_ACTUAL, NIVEL_ABASTECIMIENTO) values (3, 86, 26, 'PRENDAS_VESTIR', 94, 62, 44);
insert into ESTANTE (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANTIDAD_PRODUCTOS, CANT_ACTUAL, NIVEL_ABASTECIMIENTO) values (4, 85, 15, 'ELECTRODOMESTICOS', 8, 6, 22);
insert into ESTANTE (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANTIDAD_PRODUCTOS, CANT_ACTUAL, NIVEL_ABASTECIMIENTO) values (5, 95, 13, 'ASEO', 22, 8, 5);
insert into ESTANTE (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANTIDAD_PRODUCTOS, CANT_ACTUAL, NIVEL_ABASTECIMIENTO) values (6, 94, 58, 'ABARROTES', 88, 8, 29);
insert into ESTANTE (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANTIDAD_PRODUCTOS, CANT_ACTUAL, NIVEL_ABASTECIMIENTO) values (7, 83, 11, 'MUEBLES', 82, 9, 69);
insert into ESTANTE (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANTIDAD_PRODUCTOS, CANT_ACTUAL, NIVEL_ABASTECIMIENTO) values (8, 77, 16, 'ABARROTES', 21, 27, 6);
insert into ESTANTE (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANTIDAD_PRODUCTOS, CANT_ACTUAL, NIVEL_ABASTECIMIENTO) values (9, 81, 59, 'ELECTRODOMESTICOS', 19, 67, 81);
insert into ESTANTE (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANTIDAD_PRODUCTOS, CANT_ACTUAL, NIVEL_ABASTECIMIENTO) values (10, 43, 79, 'ELECTRODOMESTICOS', 1, 35, 1);





insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) values (1, 'isotretinoin', 4.8, 158, 'NO_PERECEDERO', 'Rolfson, Heathcote and Mitchell', 27812, 2163, 'xtkpenrrlpetfryltwocuynhtnlmx', 'Amnesteem', 'ml');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) values (2, 'Cocklebur', 4.7, 830, 'NO_PERECEDERO', 'Paucek-Douglas', 40809, 1839, 'oovpyxcpkghdffqwosrqxxrlfvclk', 'Cocklebur', 'gr');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) values (3, 'benzoyl peroxide', 1.7, 308, 'CONGELADOS', 'Gottlieb Inc', 17642, 4817, 'ladrrugjochwbqdvdbgbgngqryvmk', 'Perrigo Benzoyl Peroxide', 'gr');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) values (4, 'WORMWOOD', 3.0, 597, 'NO_PERECEDERO', 'Gusikowski-Beier', 14058, 537, 'esstiezcgftckarmhwglhzmoazzna', 'Artemisia No Smoking', 'ml');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) values (5, 'CHLORPHENIRAMINE MALEATE', 1.8, 921, 'NO_PERECEDERO', 'Turcotte-Rippin', 3355, 4000, 'ebzceeumyumoaybazqyayytfvethn', 'Clear Choice Allergy Relief', 'gr');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) values (6, 'Magesium Citrate', 3.1, 911, 'ABARROTES', 'Stark and Sons', 15377, 360, 'atqjdjqyqwgxbpcxyygflwwmngrto', 'citroma', 'gr');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) values (7, 'Loratadine', 3.1, 107, 'ABARROTES', 'Thiel, Murray and Rosenbaum', 31549, 2548, 'hcbjclmhggnoxexoslxmuhsitwafr', 'berkley and jensen loratadine', 'gr');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) values (8, 'NITROFURANTOIN', 2.1, 111, 'NO_PERECEDERO', 'Donnelly-Herzog', 33836, 643, 'gclrveqdeoetycvshiihzhzhewfho', 'NITROFURANTOIN', 'ml');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) values (9, 'Amlodipine Besylate', 3.3, 269, 'ASEO', 'Deckow Inc', 4879, 136, 'txcxnvcxekbbutcbmnafwxomyihbo', 'Amlodipine Besylate', 'gr');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) values (10, 'Salicylic Acid', 1.9, 282, 'CONGELADOS', 'Daniel and Sons', 27984, 1186, 'gufmdzzfmejnlrozvbzyebwzjfygw', 'salicylic acid', 'gr');


insert into INSUMO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA, PESO_EMP, VOL_EMP, NVL_REOR_SUCUR) values (1, 'Lovastatin', 1.2, 859, 'ASEO', 'Blanda, Sipes and Hauck', 4964, 728, 'txomufhkkleoubtwmlefpyjbeyfjn', 'Lovastatin', 'ml', 2, 1, 27);
insert into INSUMO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA, PESO_EMP, VOL_EMP, NVL_REOR_SUCUR) values (2, 'Trolamine Salicylate', 4.1, 757, 'ASEO', 'Jerde LLC', 20906, 422, 'pnmooeahqpeermgpapugydzlqoneg', 'smart sense analgesic', 'ml', 7, 4, 37);
insert into INSUMO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA, PESO_EMP, VOL_EMP, NVL_REOR_SUCUR) values (3, 'Guaifenesin and Codeine Phosphate', 4.3, 896, 'PERECEDERO', 'Christiansen Group', 4288, 1624, 'qbcnjnughbtnuisptvvoubthnybds', 'Guaifenesin and Codeine Phosphate', 'gr', 1, 4, 12);
insert into INSUMO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA, PESO_EMP, VOL_EMP, NVL_REOR_SUCUR) values (4, 'Sodium Fluoride', 2.2, 804, 'HERRAMIENTAS', 'Koch-Cremin', 23907, 2281, 'mvvewqucwwzariqfchzlmsjkohylo', 'ZOOBY', 'ml', 2, 4, 36);
insert into INSUMO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA, PESO_EMP, VOL_EMP, NVL_REOR_SUCUR) values (5, 'OCTINOXATE, ZINC OXIDE, OCTISALATE, TITANIUM DIOXIDE', 1.7, 918, 'HERRAMIENTAS', 'Ritchie-Huels', 12202, 4763, 'ndfyijeekisahccbqjmzlhbvtiegn', 'THE FIRST SUN', 'gr', 3, 4, 24);
insert into INSUMO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA, PESO_EMP, VOL_EMP, NVL_REOR_SUCUR) values (6, 'Oxygen', 2.1, 818, 'NO_PERECEDERO', 'Watsica-Hartmann', 5143, 1632, 'fgknedklnwqjvivvlmuojczmwsnoy', 'Oxygen', 'gr', 3, 1, 30);
insert into INSUMO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA, PESO_EMP, VOL_EMP, NVL_REOR_SUCUR) values (7, 'ALTERNARIA TENUIS', 4.6, 16, 'ELECTRODOMESTICOS', 'Prohaska Group', 32642, 564, 'uipzozpeootyqvnddxovdhxptxtdo', 'ALTERNARIA TENUIS', 'ml', 2, 2, 29);
insert into INSUMO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA, PESO_EMP, VOL_EMP, NVL_REOR_SUCUR) values (8, 'DIVALPROEX SODIUM', 2.6, 565, 'NO_PERECEDERO', 'Stamm, Hilpert and Runolfsson', 28252, 3387, 'sajascgrhzvwactxdihccgmikyhkh', 'DIVALPROEX SODIUM', 'gr', 10, 4, 15);
insert into INSUMO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA, PESO_EMP, VOL_EMP, NVL_REOR_SUCUR) values (9, 'Horse Epithelium', 1.3, 957, 'CONGELADOS', 'Tremblay LLC', 15389, 1925, 'efflbfdwcxatbvmgurkzbpllfdxsw', 'Horse Epithelium', 'gr', 7, 1, 33);
insert into INSUMO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA, PESO_EMP, VOL_EMP, NVL_REOR_SUCUR) values (10, 'hydromorphone hydrochloride', 1.7, 640, 'NO_PERECEDERO', 'Flatley-Crooks', 35386, 1579, 'coxlsbmriawontylhzlxpmybdpawm', 'Hydromorphone Hydrochloride', 'gr', 3, 3, 6);


insert into PROVEEDOR (NIT, NOMBRE) values (1, 'Meevee');
insert into PROVEEDOR (NIT, NOMBRE) values (2, 'Zooxo');
insert into PROVEEDOR (NIT, NOMBRE) values (3, 'Browsetype');
insert into PROVEEDOR (NIT, NOMBRE) values (4, 'Dynazzy');
insert into PROVEEDOR (NIT, NOMBRE) values (5, 'Realpoint');
insert into PROVEEDOR (NIT, NOMBRE) values (6, 'Yodel');
insert into PROVEEDOR (NIT, NOMBRE) values (7, 'Twitternation');
insert into PROVEEDOR (NIT, NOMBRE) values (8, 'Cogibox');
insert into PROVEEDOR (NIT, NOMBRE) values (9, 'Brainlounge');
insert into PROVEEDOR (NIT, NOMBRE) values (10, 'Browsedrive');

insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (1, 'Richie Mertsching', 'rmertsching0@java.com', 637,'41345 Ruskin Lane', 'NATURAL');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (2, 'Gnni Snasdell', 'gsnasdell1@biblegateway.com', 332,'1 Ridgeview Park', 'EMPRESA');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (3, 'Dalli Testin', 'dtestin2@bandcamp.com', 639,'7189 Roth Drive', 'NATURAL');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (4, 'Gay Jewkes', 'gjewkes3@flickr.com', 350, '47617 Golf Junction', 'EMPRESA');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (5, 'Darya Vuitte', 'dvuitte4@unicef.org', 216,'4390 High Crossing Hill', 'EMPRESA');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (6, 'D''arcy Kubal', 'dkubal5@vkontakte.ru', 65,'9674 Mandrake Lane', 'NATURAL');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (7, 'Mallory Girardini', 'mgirardini6@tmall.com', 462,'9 Donald Plaza', 'EMPRESA');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (8, 'Maury Gillson', 'mgillson7@slate.com', 444, '698 Nova Pass', 'EMPRESA');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (9, 'Juline Kettlewell', 'jkettlewell8@microsoft.com', 329,'77 Anderson Avenue', 'EMPRESA');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (10, 'Noel Witherington', 'nwitherington9@opensource.org', 657,'4366 Sundown Street', 'NATURAL');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (11, 'SEACOR Marine Holdings Inc.', 'mrow0@harvard.edu', 425, '55 Vernon Alley', 'EMPRESA');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (12, 'Constellium N.V.', 'fdugall1@seattletimes.com', 220, '08457 Hanover Trail', 'NATURAL');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (13, 'Yulong Eco-Materials Limited', 'tinder2@merriam-webster.com', 570, '8 Larry Road', 'EMPRESA');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (14, 'Global Ship Lease, Inc.', 'rbossel3@prlog.org', 564, '95 Vermont Crossing', 'NATURAL');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (15, 'GTY Technology Holdings, Inc.', 'hgellion4@cdc.gov', 556, '0538 Saint Paul Avenue', 'EMPRESA');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (16, 'MTGE Investment Corp.', 'fvanvuuren5@cisco.com', 201, '141 Scott Terrace', 'NATURAL');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (17, 'Momenta Pharmaceuticals, Inc.', 'tfagge6@hugedomains.com', 34, '777 Golf Course Street', 'NATURAL');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (18, 'Reading International Inc', 'mtrenbay7@posterous.com', 61, '8937 Surrey Plaza', 'EMPRESA');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (19, 'Knowles Corporation', 'dida8@google.fr', 53, '63 Spaight Road', 'NATURAL');
insert into CONSUMIDOR (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION, TIPO) values (20, 'StoneMor Partners L.P.', 'thousiaux9@discuz.net', 107, '5372 Lindbergh Avenue', 'EMPRESA');



insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (1, 14541, 'PROCESO', 4578, 9,'13/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (2, 38094, 'FINALIZADA', 795, 3, '17/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (3, 29177, 'FINALIZADA', 1843, 1,'13/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (4, 38739, 'PROCESO', 687, 1, '17/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (5, 37888, 'FINALIZADA', 2149, 1,'13/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (6, 43679, 'PROCESO', 3176, 5, '17/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (7, 7965, 'PROCESO', 4092, 10,'13/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (8, 6487, 'PROCESO', 3500, 2, '17/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (9, 31948, 'FINALIZADA', 3744, 8,'13/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (10, 35748, 'PROCESO', 1921, 8, '17/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (11, 28459, 'PROCESO', 737, 4,'13/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (12, 3537, 'PROCESO', 3513, 10, '17/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (13, 26056, 'PROCESO', 3178, 6,'13/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (14, 29751, 'FINALIZADA', 1938, 7, '17/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (15, 38004, 'PROCESO', 5034, 8,'13/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (16, 28528, 'FINALIZADA', 14, 7, '17/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (17, 47342, 'FINALIZADA', 5557, 7,'13/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (18, 13776, 'FINALIZADA', 4693, 6, '17/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (19, 45801, 'PROCESO', 4022, 9,'13/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (20, 41557, 'PROCESO', 1781, 4, '17/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (21, 45584, 'PROCESO', 3958, 3,'13/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (22, 45238, 'PROCESO', 4343, 2, '17/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (23, 21124, 'FINALIZADA', 4112, 6,'13/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (24, 18177, 'PROCESO', 1233, 8,'13/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (25, 4653, 'FINALIZADA', 1299, 4,'13/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (26, 41599, 'PROCESO', 3754, 4, '17/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (27, 28344, 'FINALIZADA', 502, 5, '17/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (28, 25084, 'FINALIZADA', 19, 1,'13/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (29, 14395, 'PROCESO', 1067, 6, '17/10/2018');
insert into VENTA (ID, COSTO_TOTAL, ESTADO, PUNTOS, ID_CONSUM, FECHA) values (30, 46937, 'PROCESO', 4608, 5,'13/10/2018');




insert into PROMOCION (ID, ESTADO, NOMBRE, ESPECIFICACIONES, TIPO, FECHA_INI, FECHA_FIN, INSUMO, CANT_PRODUCTOS) values (1, 'zmeqll', 'Aleksandr', 'blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede', 5, '19/10/2017', '28/10/2018',3,2);
insert into PROMOCION (ID, ESTADO, NOMBRE, ESPECIFICACIONES, TIPO, FECHA_INI, FECHA_FIN, INSUMO, CANT_PRODUCTOS)values (2, 'dgjnrf', 'Barron', 'lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin', 4, '05/10/2017', '12/10/2018',6,6);
insert into PROMOCION (ID, ESTADO, NOMBRE, ESPECIFICACIONES, TIPO, FECHA_INI, FECHA_FIN, INSUMO, CANT_PRODUCTOS)values (3, 'rlhdws', 'Vaclav', 'cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum', 1, '12/10/2017', '19/10/2018',10, 11);
insert into PROMOCION (ID, ESTADO, NOMBRE, ESPECIFICACIONES, TIPO, FECHA_INI, FECHA_FIN, INSUMO, CANT_PRODUCTOS)values (4, 'yzmhfo', 'Cyril', 'morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel nisl duis', 1, '03/10/2017', '25/10/2018',9,12 );
insert into PROMOCION (ID, ESTADO, NOMBRE, ESPECIFICACIONES, TIPO, FECHA_INI, FECHA_FIN, INSUMO, CANT_PRODUCTOS)values (5, 'dvkaea', 'Dannel', 'vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices', 1, '27/10/2017', '26/10/2018',4,15);
insert into PROMOCION (ID, ESTADO, NOMBRE, ESPECIFICACIONES, TIPO, FECHA_INI, FECHA_FIN, INSUMO, CANT_PRODUCTOS)values (6, 'txlxze', 'Deck', 'vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio', 1, '22/10/2017', '17/10/2018',6,11);
insert into PROMOCION (ID, ESTADO, NOMBRE, ESPECIFICACIONES, TIPO, FECHA_INI, FECHA_FIN, INSUMO, CANT_PRODUCTOS)values (7, 'vzcika', 'Whitby', 'ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien', 5, '05/10/2017', '26/10/2018',1,11);
insert into PROMOCION (ID, ESTADO, NOMBRE, ESPECIFICACIONES, TIPO, FECHA_INI, FECHA_FIN, INSUMO, CANT_PRODUCTOS)values (8, 'czmlum', 'Aloysius', 'pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus', 4, '13/10/2017', '12/10/2018',7,12);
insert into PROMOCION (ID, ESTADO, NOMBRE, ESPECIFICACIONES, TIPO, FECHA_INI, FECHA_FIN, INSUMO, CANT_PRODUCTOS)values (9, 'fsfffl', 'Yanaton', 'vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia', 1, '15/10/2017', '09/10/2018',8,15);
insert into PROMOCION (ID, ESTADO, NOMBRE, ESPECIFICACIONES, TIPO, FECHA_INI, FECHA_FIN, INSUMO, CANT_PRODUCTOS)values (10, 'cktxip', 'Jefferey', 'viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra', 5, '19/10/2017', '13/10/2018',9,11);
insert into PROMOCION (ID, ESTADO, NOMBRE, ESPECIFICACIONES, TIPO, FECHA_INI, FECHA_FIN, INSUMO, CANT_PRODUCTOS)values (11, 'ttamme', 'Manolo', 'ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque', 1, '06/10/2017', '14/10/2018',2,11);
insert into PROMOCION (ID, ESTADO, NOMBRE, ESPECIFICACIONES, TIPO, FECHA_INI, FECHA_FIN, INSUMO, CANT_PRODUCTOS)values (12, 'rkywzt', 'Sydney', 'nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros', 1, '10/10/2017', '24/10/2018',1,12);
insert into PROMOCION (ID, ESTADO, NOMBRE, ESPECIFICACIONES, TIPO, FECHA_INI, FECHA_FIN, INSUMO, CANT_PRODUCTOS)values (13, 'lrtxbc', 'Harv', 'elit proin risus praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis in', 4, '19/10/2017', '23/10/2018',6,15);
insert into PROMOCION (ID, ESTADO, NOMBRE, ESPECIFICACIONES, TIPO, FECHA_INI, FECHA_FIN, INSUMO, CANT_PRODUCTOS)values (14, 'drymxx', 'Hughie', 'ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida', 3, '06/10/2017', '26/10/2018',3,26);
insert into PROMOCION (ID, ESTADO, NOMBRE, ESPECIFICACIONES, TIPO, FECHA_INI, FECHA_FIN, INSUMO, CANT_PRODUCTOS)values (15, 'ijibvm', 'Bryn', 'varius ut blandit non interdum in ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere', 2, '22/10/2017', '17/10/2018',5,69);





insert into PEDIDO (ID, COSTO, NIT_PROV, COD_PRODUCTO, ESTADO, FECHA) values (1, 25158, 6, 10, 'recibido', '14/10/2018');
insert into PEDIDO (ID, COSTO, NIT_PROV, COD_PRODUCTO, ESTADO, FECHA) values (2, 17249, 2, 7, 'proceso', '11/10/2018');
insert into PEDIDO (ID, COSTO, NIT_PROV, COD_PRODUCTO, ESTADO, FECHA) values (3, 2077, 3, 1, 'recibido', '12/10/2018');
insert into PEDIDO (ID, COSTO, NIT_PROV, COD_PRODUCTO, ESTADO, FECHA) values (4, 17727, 2, 7, 'recibido', '24/10/2018');
insert into PEDIDO (ID, COSTO, NIT_PROV, COD_PRODUCTO, ESTADO, FECHA) values (5, 29650, 4, 8, 'proceso', '20/10/2018');
insert into PEDIDO (ID, COSTO, NIT_PROV, COD_PRODUCTO, ESTADO, FECHA) values (6, 21203, 3, 5, 'recibido', '22/10/2018');
insert into PEDIDO (ID, COSTO, NIT_PROV, COD_PRODUCTO, ESTADO, FECHA) values (7, 28592, 3, 7, 'recibido', '15/10/2018');
insert into PEDIDO (ID, COSTO, NIT_PROV, COD_PRODUCTO, ESTADO, FECHA) values (8, 22537, 7, 6, 'recibido', '10/10/2018');
insert into PEDIDO (ID, COSTO, NIT_PROV, COD_PRODUCTO, ESTADO, FECHA) values (9, 33266, 5, 7, 'recibido', '30/10/2018');
insert into PEDIDO (ID, COSTO, NIT_PROV, COD_PRODUCTO, ESTADO, FECHA) values (10, 20669, 9, 2, 'recibido', '27/10/2018');


insert into CARRITO (ID, ID_CONSUM) values (1, 10);
insert into CARRITO (ID, ID_CONSUM) values (2, 9);
insert into CARRITO (ID, ID_CONSUM) values (3, 2);
insert into CARRITO (ID, ID_CONSUM) values (4, 20);
insert into CARRITO (ID, ID_CONSUM) values (5, 8);
insert into CARRITO (ID, ID_CONSUM) values (6, 3);
insert into CARRITO (ID, ID_CONSUM) values (7, 7);
insert into CARRITO (ID, ID_CONSUM) values (8, 13);
insert into CARRITO (ID, ID_CONSUM) values (9, 8);
insert into CARRITO (ID, ID_CONSUM) values (10, 1);
insert into CARRITO (ID, ID_CONSUM) values (11, 1);
insert into CARRITO (ID, ID_CONSUM) values (12, 18);
insert into CARRITO (ID, ID_CONSUM) values (13, 15);
insert into CARRITO (ID, ID_CONSUM) values (14, 5);
insert into CARRITO (ID, ID_CONSUM) values (15, 18);




insert into PRODUCTOS_VENDIDOS (COD_PRODUCTO, ID_VENTA) values (1, 1);
insert into PRODUCTOS_VENDIDOS (COD_PRODUCTO, ID_VENTA) values (1, 5);
insert into PRODUCTOS_VENDIDOS (COD_PRODUCTO, ID_VENTA) values (1, 10);
insert into PRODUCTOS_VENDIDOS (COD_PRODUCTO, ID_VENTA) values (5, 5);
insert into PRODUCTOS_VENDIDOS (COD_PRODUCTO, ID_VENTA) values (3, 5);
insert into PRODUCTOS_VENDIDOS (COD_PRODUCTO, ID_VENTA) values (7, 8);
insert into PRODUCTOS_VENDIDOS (COD_PRODUCTO, ID_VENTA) values (3, 4);
insert into PRODUCTOS_VENDIDOS (COD_PRODUCTO, ID_VENTA) values (3, 7);
insert into PRODUCTOS_VENDIDOS (COD_PRODUCTO, ID_VENTA) values (7, 7);
insert into PRODUCTOS_VENDIDOS (COD_PRODUCTO, ID_VENTA) values (8, 10);
insert into PRODUCTOS_VENDIDOS (COD_PRODUCTO, ID_VENTA) values (1, 4);
insert into PRODUCTOS_VENDIDOS (COD_PRODUCTO, ID_VENTA) values (10, 7);
insert into PRODUCTOS_VENDIDOS (COD_PRODUCTO, ID_VENTA) values (3, 6);
insert into PRODUCTOS_VENDIDOS (COD_PRODUCTO, ID_VENTA) values (5, 4);
insert into PRODUCTOS_VENDIDOS (COD_PRODUCTO, ID_VENTA) values (4, 3);


insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (1, 10);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (8, 1);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (1, 9);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (7, 8);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (6, 4);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (8, 8);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (7, 9);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (4, 1);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (2, 8);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (1, 1);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (8, 4);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (7, 3);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (8, 4);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (3, 1);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (1, 3);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (6, 8);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (3, 9);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (10, 7);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (6, 2);
insert into PROVEEDOR_PRODUCTO (ID_PRODUCTO, NIT_PROV) values (9, 1);


insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (2, 2);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (5, 2);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (14, 5);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (12, 4);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (4, 8);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (4, 6);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (4, 3);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (13, 8);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (2, 10);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (1, 1);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (1, 9);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (7, 6);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (9, 3);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (11, 7);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (7, 3);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (9, 6);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (1, 2);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (1, 4);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (12, 8);
insert into CARRITO_INSUM (ID_CARRITO, ID_INSUM) values (11, 1);


insert into BODEGA_INSUMO (CODIGO_BARRAS_INS, ID_BOD) values (2, 8);
insert into BODEGA_INSUMO (CODIGO_BARRAS_INS, ID_BOD) values (5, 9);
insert into BODEGA_INSUMO (CODIGO_BARRAS_INS, ID_BOD) values (6, 5);
insert into BODEGA_INSUMO (CODIGO_BARRAS_INS, ID_BOD) values (7, 9);
insert into BODEGA_INSUMO (CODIGO_BARRAS_INS, ID_BOD) values (1, 4);
insert into BODEGA_INSUMO (CODIGO_BARRAS_INS, ID_BOD) values (4, 7);
insert into BODEGA_INSUMO (CODIGO_BARRAS_INS, ID_BOD) values (1, 4);
insert into BODEGA_INSUMO (CODIGO_BARRAS_INS, ID_BOD) values (8, 2);
insert into BODEGA_INSUMO (CODIGO_BARRAS_INS, ID_BOD) values (1, 7);
insert into BODEGA_INSUMO (CODIGO_BARRAS_INS, ID_BOD) values (9, 3);


insert into ESTANTE_INSUMO (CODIGO_BARRAS_INS, ID_EST) values (5, 3);
insert into ESTANTE_INSUMO (CODIGO_BARRAS_INS, ID_EST) values (5, 4);
insert into ESTANTE_INSUMO (CODIGO_BARRAS_INS, ID_EST) values (4, 8);
insert into ESTANTE_INSUMO (CODIGO_BARRAS_INS, ID_EST) values (2, 10);
insert into ESTANTE_INSUMO (CODIGO_BARRAS_INS, ID_EST) values (6, 3);
insert into ESTANTE_INSUMO (CODIGO_BARRAS_INS, ID_EST) values (8, 2);
insert into ESTANTE_INSUMO (CODIGO_BARRAS_INS, ID_EST) values (7, 9);
insert into ESTANTE_INSUMO (CODIGO_BARRAS_INS, ID_EST) values (4, 6);
insert into ESTANTE_INSUMO (CODIGO_BARRAS_INS, ID_EST) values (2, 1);
insert into ESTANTE_INSUMO (CODIGO_BARRAS_INS, ID_EST) values (3, 2);

COMMIT;
