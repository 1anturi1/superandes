--RF1
insert into PROVEEDOR (NIT, NOMBRE) values (11, 'Yefersonco');
--RF2
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) 
values (11, 'Papas margarita', 4.8, 158, 'NO_PERECEDERO', 'Lays', 27812, 2163, 'xtkpenrrlpetfryltwocuynhtnlmx', 'galguerias', 'gr');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA)
values (12, 'kellogs', 4.7, 830, 'NO_PERECEDERO', 'Nestle', 40809, 1839, 'oovpyxcpkghdffqwosrqxxrlfvclk', 'empaquetado', 'gr');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) 
values (13, 'atun', 4.0, 500, 'NO_PERECEDERO', 'El mar', 5000, 2163, 'xtkpenrrlpetfryltwocuynhtnlmx', 'enlatados', 'ml');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) 
values (14, 'arroz', 3.7, 500, 'NO_PERECEDERO', 'Diana', 40809, 1839, 'oovpyxcpkghdffqwosrqxxrlfvclk', 'granos', 'gr');


insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) 
values (15, 'Pechuga de pollo', 3.5, 308, 'CONGELADOS', 'Gottlieb Inc', 13000, 4817, 'ladrrugjochwbqdvdbgbgngqryvmk', 'carnes blancas', 'gr');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) 
values (16, 'Carne fresca', 2.5, 308, 'CONGELADOS', 'Gottlieb Inc', 17642, 4817, 'ladrrugjochwbqdvdbgbgngqryvmk', 'carnes rojas', 'gr');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) 
values (17, 'Nuggets', 3.9, 308, 'CONGELADOS', 'Gottlieb Inc', 20000, 4817, 'ladrrugjochwbqdvdbgbgngqryvmk', 'precocidos', 'gr');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) 
values (18, 'papas precocidas', 4.9, 308, 'CONGELADOS', 'Papasarsistrans', 15000, 4817, 'ladrrugjochwbqdvdbgbgngqryvmk', 'papas', 'gr');


insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) 
values (19, 'Esponja Bobsiana', 3.1, 911, 'ABARROTES', 'Stark and Sons', 38000, 360, 'atqjdjqyqwgxbpcxyygflwwmngrto', 'lavaloza', 'gr');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) 
values (20, 'Trapero gris', 3.1, 911, 'ABARROTES', 'Industrias Rand', 30000, 360, 'atqjdjqyqwgxbpcxyygflwwmngrto', 'aseo interior', 'gr');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) 
values (21, 'Escoba de metal', 3.1, 911, 'ABARROTES', 'Industrias Wayne', 25000, 360, 'atqjdjqyqwgxbpcxyygflwwmngrto', 'aseo exterior', 'gr');
insert into PRODUCTO (CODIGO_BARRAS, NOMBRE, CALIFICACION_CALIDAD, CANTIDAD_PRESENT, CLASIFICACION, MARCA, PRECIO, PRECIO_UNIT, PRESENTACION, TIPO, UNIT_MEDIDA) 
values (22, 'Recogedor milagros', 3.1, 911, 'ABARROTES', 'Stark Industries', 60000, 360, 'atqjdjqyqwgxbpcxyygflwwmngrto', 'aseo basico', 'gr');

--RF3
insert into CONSUMIDOR_NAT (CEDULA, NOMBRE, CORREO, PUNTOS) values (25, 'Darcy Kubal', 'dkubal5@vkontakte.ru', 1);
insert into CONSUMIDOR_EMP (NIT, NOMBRE, CORREO, PUNTOS, DIRECCION) values (366, 'Wayne, Inc.', 'rbossel3@prlog.org', 1, '95 Vermont Crossing');

--RF4
insert into SUCURSAL (id, NOMBRE, CIUDAD, DIRECCION) values (16, 'Sucursal 03', 'Gotica', '569 Hells Kitchen');

--RF5
insert into SUCURSALES_SUPERMERC (id_SUPERMERCADO, ID_SUCURSAL) values (5, 16);

--RF6
insert into ESTANTE (ID, CAPACIDAD_PESO, CAPACIDAD_VOLUMEN, CATEGORIA_PRODUCTO, CANTIDAD_PRODUCTOS, CANT_ACTUAL, NIVEL_ABASTECIMIENTO) values (20, 94, 58, 'ABARROTES', 150, 1, 88);
insert into ESTANTES_SUCURSAL (ID_ESTANTE, ID_SUCURSAL) values (20, 16);



