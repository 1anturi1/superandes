--rfc10
SELECT
    *
FROM
    (
        SELECT
            COUNT(A2.COD_PRODUCTO) AS CANTIDAD_COMPRADA,
            A2.COD_PRODUCTO,
            A2.ID_CONSUM
        FROM
            (
                SELECT
                    E1.COD_PRODUCTO,
                    E2.ID_CONSUM
                FROM
                    PRODUCTOS_VENDIDOS E1
                    INNER JOIN VENTA E2 ON E1.ID_VENTA = E2.ID
                WHERE
                --ID DE LA SUCURSAL DEL ADMINISTRADOR
                E2.ID_SUCURSAL = 158 
                
                AND
                      E2.FECHA BETWEEN
--fecha inicial param1
                     '1/01/2017' AND 
--fecha final param2 
                     '17/10/2018'
            ) A2
        GROUP BY
            A2.COD_PRODUCTO,
            A2.ID_CONSUM
      
    ) F1
    INNER JOIN CONSUMIDOR F3 ON F3.NIT = F1.ID_CONSUM
WHERE
    F1.COD_PRODUCTO =
--codigo del producto param3
     358;







SELECT
            COUNT(A2.COD_PRODUCTO) AS CANTIDAD_COMPRADA,
            A2.COD_PRODUCTO,
            A2.ID_CONSUM
        FROM
            (
                SELECT
                    E1.COD_PRODUCTO,
                    E2.ID_CONSUM
                FROM
                    PRODUCTOS_VENDIDOS E1
                    INNER JOIN VENTA E2 ON E1.ID_VENTA = E2.ID
                WHERE
                --ID DE LA SUCURSAL DEL ADMINISTRADOR
                E2.ID_SUCURSAL = 158 
                
                AND
                      E2.FECHA BETWEEN
--fecha inicial param1
                     '13/10/2017' AND 
--fecha final param2 
                     '17/10/2018'
            ) A2
        GROUP BY
            A2.COD_PRODUCTO,
            A2.ID_CONSUM  ;










--rfc11
 SELECT*
FROM
    (
        SELECT
            COUNT(A2.COD_PRODUCTO) AS CANTIDAD_COMPRADA,
            A2.COD_PRODUCTO,
            A2.ID_CONSUM
        FROM
            (
                SELECT
                    E1.COD_PRODUCTO,
                    E2.ID_CONSUM
                FROM
                    PRODUCTOS_VENDIDOS E1
                    INNER JOIN VENTA E2 ON E1.ID_VENTA = E2.ID
                WHERE
                --ID DE LA SUCURSAL DEL ADMINISTRADOR
                E2.ID_SUCURSAL = 158 
                
                AND
                      E2.FECHA BETWEEN
--fecha inicial param1
                     '13/10/2017' AND 
--fecha final param2 
                     '17/10/2018'
            ) A2
        GROUP BY
            A2.COD_PRODUCTO,
            A2.ID_CONSUM
    ) F1
    INNER JOIN CONSUMIDOR F3 ON F3.NIT = F1.ID_CONSUM
WHERE
    F1.COD_PRODUCTO =
--codigo del producto param3
     358;